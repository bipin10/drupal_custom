<?php
/**
 * @file
 * Callback for viewing entities
 */
/**
 * show list of existing entites
 */
function travel_list()
{
    drupal_set_title(t('Travels List'));
    // show link to the entity "add" page
    $output = '<br/>';
    $output .= theme_link(
        [
            'text' => t('Add New Travel'),
            'path' => 'travel/add',
            'options' => [
                'attributes' => [],
                'html' => true,
            ],
        ]
    );
    // show list of existing entities
    $entities = entity_load('travel');

    $items = array();
    foreach ($entities as $entity) {
        $entity_uri = entity_uri('travel', $entity);
        $items[] = l(entity_label('travel', $entity), $entity_uri['path']);
    }
    $output .= theme_item_list(
        [
            'items' => $items,
            'title' => t('Travels list'),
            'type' => 'ul',
            'attributes' => [],
        ]
    );
    return $output;
}

/**
 * Entity view callback
 */
function travel_view($entity){
    drupal_set_title(entity_label('travel',$entity));
    // return automatically generated view page
    return entity_view(
        'travel',
        [entity_id('travel',$entity) => $entity],
        'full'
    );
}
