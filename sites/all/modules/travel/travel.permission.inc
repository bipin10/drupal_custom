<?php
/**
 * permissions
 *
 */

function travel_permission()
{
    $permissions = [
        'view travel content' => [
            'title' => t('View Typical Entity Example content'),
        ],
        'administer travel content' => [
            'title' => t('Administer Typical Entity Example content'),
            'restrict access' => true,
        ],
    ];
    return $permissions;
}
