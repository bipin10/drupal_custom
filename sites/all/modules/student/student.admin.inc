<?php

/**
 * @return mixed
 * Function to view the tableview of students.
 * The tableview has sorting of fields and has pagination as well
 * Called from student.module
 */
function student_list_page()
{
    global $base_url;
    // access check
    $list_access = student_access('list');

    $header = [
        'id' => ['data' => t('Id'), 'field' => 's.id', 'sort' => 'asc', ],
        'fname' => ['data' => t('First Name'), 'field' => 's.firstname'],
        'lname' => ['data' => t('Last Name'), 'field' => 's.lastname'],
        'class' => ['data' => t('Class'), 'field' => 's.class'],
        'roll' => [
            'data' => t('Roll Number'),
            'field' => 's.rollnumber',

        ],
        'image' => ['data' => t('Image'), 'field' => 's.image'],
        'operation' => ['data' => t('Actions')],
    ];

    // $create_access = user_access(PERM_CREATE_STUDENT);
    $create_access = student_access('create');
    $view_access = student_access('view');
    $edit_access = student_access('edit');
    $delete_access = student_access('delete');
    //Add action link
    $add_link_url = l(t('Add Student'), 'students/create');
    if ($create_access) {
        $form['add-action'] = [
            '#markup' => '<div class="action-wrapper"><ul class="action-links"><li>' . $add_link_url . '</li></ul></div>',
        ];
    }

    $query = db_select(STUDENT_BASE_TABLE, 's')
        ->fields('s');
    $query = $query->extend('PagerDefault')
        ->extend('TableSort')
        ->orderByHeader($header);
    $query->limit(5);
    $students = $query->execute()->fetchAll();

    $rows = [];
    // $students = entity_load('student');
    foreach ($students as $student) {

        $st = student_load($student->id);

        $editlink = null;
        $deletelink = null;
        $view_link = l(t('View'), $base_url . '/students/' . $student->id . '/details');
        if ($edit_access) {
            $editlink = l(t('Edit'), $base_url . '/students/' . $student->id . '/edit');
        }
        if ($delete_access) {
            $deletelink = l(t('Delete'), $base_url . '/students/' . $student->id . '/delete');
        }
        $url = '';
        // if (!empty($student->image)) {
        //     $uri = file_load($student->image)->uri;
        //     $url = file_create_url($uri);
        // }
        $image_data = field_get_items('student', $st, 'student_header_image');
        if (isset($image_data[0]['uri'])) {
            $image_uri = $image_data[0]['uri'];
        } else {
            $image_uri = 'public://student/blank.png';
        }

        $url = file_create_url($image_uri);

        $rows[] = [
            'id' => ['data' => $student->id],
            'fname' => ['data' => $student->firstname],
            'lname' => ['data' => $student->lastname],
            'class' => ['data' => $student->class],
            'roll' => ['data' => $student->rollnumber],
            'image' => ['data' => '<img src="' . $url . '" alt="No Image" height="100" width="100" />'],
            'operation' => ['data' => $view_link . '&nbsp;&nbsp;&nbsp;' . $editlink . '&nbsp;&nbsp;&nbsp;' . $deletelink],
        ];
    }
    if ($list_access) {
        $form['students'] = [
            '#theme' => 'table',
            '#header' => $header,
            '#rows' => $rows,
            '#empty' => t('No students available.'),
        ];
    }


    $form['pager']['#markup'] = theme('pager');
    return $form;
}
