<?php

//Define all the constant used for the module

define('STUDENT_BASE_TABLE', 'student');
define('STUDENT_ENTITY', 'student');

define('PERM_ADMINISTER_STUDENT', 'administer students');
define('PERM_LIST_STUDENT', 'list students');
define('PERM_CREATE_STUDENT', 'create students');
define('PERM_VIEW_STUDENT', 'view students');
define('PERM_EDIT_STUDENT', 'edit students');
define('PERM_DELETE_STUDENT', 'delete students');