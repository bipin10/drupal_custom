<?php

/**
 * typical entity and controller custom classes
 */

class StudentController extends DrupalDefaultEntityController
{

    public function create()
    {
        return (object)array(
            'id' => '',
            'firstname' => '',
            'lastname' => '',
            'rollnumber' => '',
            'type' => 'student',
        );
    }

    /** 
     * permanently save the given entity
     */
    public function save($pestle)
    {
        $transaction = db_transaction();
        try {
            global $user;
            $pestle->is_new = empty($pestle->id);

            $pestle->revision_timestamp = REQUEST_TIME;

            // Giving modules the opportunity to prepare field data for saving.
            field_attach_presave('student', $pestle);

            if ($pestle->is_new) {
                $pestle->uid = $user->uid;
                // Setting the timestamp fields.
                $pestle->created = REQUEST_TIME;
                $pestle->changed = REQUEST_TIME;
                drupal_write_record('student', $pestle);
                $op = 'insert';
            } else {
                $pestle->changed = REQUEST_TIME;
                // Saving the updated status.
                drupal_write_record('student', $pestle, 'id');
                $op = 'update';
            }
            // Saving fields.
            $function = 'field_attach_' . $op;
            $function('student', $pestle);
            module_invoke_all('entity_' . $op, $pestle, 'student');
            // Clearing internal properties.
            unset($pestle->is_new);
            // Ignoring slave server temporarily to give time for the saved order to be propagated to the slave.
            db_ignore_slave();
            return $pestle;
        } catch (Exception $e) {
            $transaction->rollback();
            // watchdog_exception(STUDENT_ENTITY, $e, null, WATCHDOG_ERROR);
            return false;
        }
    }

}