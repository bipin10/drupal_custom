<?php

/**
 * @param $form
 * @param $form_state
 * @param null $id
 *
 * @return mixed
 * Student Creation Form. Used for both create and edit cases.
 * Called from menu in student.module
 */
function student_create_form($form, &$form_state, $student = null)
{
    // if (!is_null($id)) {
        
        //     $student = student_fetch_individual_data($id);
        
        //     //@TODO: Check if the student exists in the system
    if (!empty($student->id)) {
        $form['id'] = [
            '#type' => 'hidden',
            '#default_value' => $student->id,
        ];

    } else {
        $student = entity_get_controller('student')->create();
    }

    $form_state['entity'] = $student;
        // else {
            //     drupal_set_message(t('Student data does not exists !!!'), 'error');
            //     return false;
            // }
            
            // }
            // if (!empty($segment)) {
                //     $student = $segment;
                // }
                // print($student->id);
                // die();
                
                //@TODO : Add a new field to select the student level (E.g. Student Class)
                
                // @TODO:Done
    $form['firstname'] = [
        '#type' => 'textfield',
        '#title' => t('First Name'),
        '#id' => 'student_firstname',
        '#suffix' => '<div class="validation-error-msg" id="student-firstname-error_msg"></div>',
        '#required' => true,
        '#default_value' => isset($student->firstname) ? $student->firstname : '',
    ];
    $form['lastname'] = [
        '#type' => 'textfield',
        '#title' => t('Last Name'),
        '#id' => 'student_lastname',
        '#suffix' => '<div class="validation-error-msg" id="student-lastname-error_msg"></div>',
        '#required' => true,
        '#default_value' => isset($student->lastname) ? $student->lastname : '',
    ];
                
                // $form['class'] = [
                    //     '#type' => 'select',
                    //     '#title' => t('Class'),
                    //     '#id' => 'student_class',
                    //     '#suffix' => '<div class="validation-error-msg" id="student-class-error_msg"></div>',
                    //     '#options' => [
                        //         '' => 'select',
                        //         'one' => 'one',
                        //         'two' => 'two',
                        //     ],
                        //     '#required' => true,
                        //     '#default_value' => isset($student->class) ? $student->class : '',
                        // ];
    $form['rollnumber'] = [
        '#type' => 'textfield',
        '#title' => t('Roll Number'),
        '#id' => 'student_rollnumber',
        '#suffix' => '<div class="validation-error-msg" id="student-rollnumber-error_msg"></div>',
        '#required' => true,
        '#default_value' => isset($student->rollnumber) ? $student->rollnumber : '',
    ];
                        
                        
                        //@TODO: Add a picture for the student
                        // $form['image'] = [
                            //     '#type' => 'managed_file',
                            //     '#name' => 'image',
                            //     '#id' => 'student_image',
                            //     '#title' => t('Student Image'),
                            //     '#size' => 40,
                            //     '#description' => t("Image should be less than 400 pixels wide"),
                            //     '#upload_location' => 'public://uploads/student/',
                            //     '#upload_validators' => [
                                //         'file_validate_extensions' => ['png jpg jpeg gif apng svg'],
                                //     ],
                                // ];
                                //@TODO: Instead of using drupal default submit, use ajax submit and ajax validation

    field_attach_form('student', $student, $form, $form_state);
    $form['submit'] = [
        '#type' => 'button',
        '#name' => 'save',
        '#value' => t('Save'),
        '#attributes' => array('class' => array('btn btn-primary')),
        '#ajax' => [
            'callback' => 'student_ajax_submit',
            'event' => 'click',
            'wrapper' => 'group_box',
        ],
    ];
    $form['actions']['cancel'] = array(
        '#type' => 'submit',
        '#value' => "Cancel",
        '#weight' => 55,
        '#submit' => array('_custom_form_cancel'),
        '#limit_validation_errors' => array(),
    );
                                // 2) Add a javascript file 
    $form['#attached']['js'][] = array(
        'type' => 'file',
        'data' => drupal_get_path('module', 'student') . '/js/custom.js',
    );
    return $form;
}
function _custom_form_cancel($form, &$form_state)
{
    drupal_goto('students');
}

/**
 * @param $form
 * @param $form_state
 * The Validation callback for the student form.
 * Note that adding "_validate" to the form above automatically calls the
 *   validation function
 *
 */
// function student_create_form_validate($form, $form_state)
// {
//     $form_data = (object)$form_state['values'];

//     $triggering_element = $form_state['triggering_element']['#name'];

//     //Triggering Element is checked to see if the user has clicked "Save" or "Cancel"
//     //The '#name' attribute above in the submit type is used
//     //Only if the user has clicked "Save", validation is performed
//     //If the user has clicked "Cancel", clear the validation errors from form and the validation message
//     if ($triggering_element == 'save') {
//         //Checking to see that if the rollnumber is non-empty and is not a number.
//         if (!empty($form_data->rollnumber) && (!ctype_digit($form_data->rollnumber))) {
//             form_set_error('rollnumber', t('Roll Number is not valid.'));
//         }
//         // if(!empty($form_data->id)){
//         //     // print(print_r($form_data,1));die();
//         //     $student=student_fetch_individual_data($form_data->id);
//         //     if( $student->rollnumber==$form_data->rollnumber){
//         //         form_set_error('rollnumber', t('Roll Number is already exist.'));
//         //     }
//     }
//     //@TODO: Check if the rollnumber already exists and is of the same user

//     else {
//         form_clear_error(); //This will only clear the error [required = TRUE] in the form above and thus message still shows up.
//         unset($_SESSION['messages']['error']); //To clear the message as well, we need to clear the error message from session
//     }
// }
/**
 * ajax test
 */
function student_ajax_submit($form, $form_state)
{
    global $base_url; //This global is the base url of the system
    try {
        /**
         * validation check
         */
        if ($form_state['values']['firstname'] == "") {
            form_set_error('firstname');
        }
        if ($form_state['values']['lastname'] == "") {
            form_set_error('lastname');
        }
        // if ($form_state['values']['class'] == "") {
        //     form_set_error('class');
        // }
        if ($form_state['values']['rollnumber'] == "") {
            form_set_error('rollnumber');
        }
        $error = form_get_errors();
        $commands = array();
        $required_fields = array('firstname', 'lastname', 'rollnumber');

        if ($error) {
        // if any error exist return this
            $commands[] = array("command" => 'loadDefaultMrd');
            foreach ($required_fields as $row) {
                if (isset($error[$row])) {
                // student-firstname-error_msg
                    $commands[] = ajax_command_invoke('#student_' . $row, 'addClass', array('error'));
                    $commands[] = ajax_command_html('#student-' . $row . '-error_msg', t('This field is required'));
                } else {
                    $commands[] = ajax_command_invoke('#student_' . $row, 'removeClass', array('error'));
                    $commands[] = ajax_command_html('#student-' . $row . '-error_msg', '');
                }
            }
        // return $element;
        } else {
            // submit logic
            global $user;
            $student = (object)$form_state['values'];
            $student_entity = $form_state['entity'];

                    // notify field widgets
            field_attach_submit('student', $student_entity, $form, $form_state);
                // save the student
            student_new($student);

            $message = t('Student updated.');
            drupal_set_message($message, 'status', false);
                // } else {
                //     drupal_set_message("Duplicate data is not allowed.", $type = 'error');
                // }
        // }
            // redirect to list on submit

            // $redirect_url = $base_url . '/students';
            // $commands[] = ajax_command_invoke(null, 'redirect', array($redirect_url));

            $path = $base_url . '/students';
            ctools_include('ajax');
            ctools_add_js('ajax-responder');
            $commands[] = ctools_ajax_command_redirect($path);
        }
    } catch (Exception $e) {
        drupal_set_message(t('Something went wrong! Try again later!'), 'error');
    }
    print ajax_render($commands);
    drupal_exit();
}
/**
 * @param $form
 * @param $form_state
 * The submit handler for the student form.
 * Note that adding "_submit" to the form automatically calls the submit
 *   function
 */
// function student_create_form_submit($form, &$form_state)
// {

//     global $base_url; //This global is the base url of the system
//     if (form_get_errors()) {
//         return $form;
//     }
//     //Checking the triggering element. Same as in validation function
//     $triggering_element = $form_state['triggering_element']['#name'];

//     if ($triggering_element == 'save') {

//         $form_data = (object)$form_state['values'];

//         //If the property "id" exists in the data, it's the update case
//         if (property_exists($form_data, 'id')) {
//             $form_data->changed = REQUEST_TIME;
//             drupal_write_record(STUDENT_BASE_TABLE, $form_data, 'id');
//             $message = t('Student Updated.');
//         } else {
//             //If the property "id" doesn't exist in the data, it's the creation case
//             $form_data->created = $form_data->changed = REQUEST_TIME;
//             drupal_write_record(STUDENT_BASE_TABLE, $form_data);
//             $message = t('Student Created.');
//         }
//         drupal_set_message($message, 'status', false);
//     }

//     drupal_goto($base_url . '/students');
// }

/**
 * @param $form
 * @param $form_state
 * @param $id
 *
 * @return mixed
 * The callback function for the student deletion.
 * The form presents a confirmation if you want to delete the student
 */
function students_delete_form($form, &$form_state, $student)
{
    $form['student'] = ['#type' => 'value', '#value' => $student];
    $title = $student->firstname . ' ' . $student->lastname;
    return confirm_form($form, t('Are you sure you want to delete %title ?', ['%title' => $title]), 'students', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * @param $form
 * @param $form_state
 * The student deletion submit callback.
 * Deletes the student data if "delete" button is pressed.
 * i.e. checks if the form is confirmed and only deletes the data if confirmed
 */
function students_delete_form_submit($form, &$form_state)
{
    global $base_url;
    if ($form_state['values']['confirm']) {
        $student = $form_state['values']['student'];
        db_delete(STUDENT_BASE_TABLE)
            ->condition('id', $student->id)
            ->execute();

        drupal_set_message(t('Student is deleted.'));
    }
    drupal_goto($base_url . '/students');
}
