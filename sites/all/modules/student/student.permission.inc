<?php

/**
 * Include the constant file
 */
module_load_include('inc', 'student', 'student.constant');

/**
 * @param $op
 * @param $student
 * @param $account
 * Implements hook_access().
 * This hook is used to define authentication and authorization for users.
 * Permissions are checked here and defined in hook_permission() function.
 */
function student_access($op, $student = null, $account = null)
{

  global $user; //This global variable is the currently logged in user

  if (!in_array($op, ['list', 'create', 'edit', 'view', 'delete'])) {
    return false;
  }

  if (is_null($account)) {
    $account = $user;
  }

  //Superadmin has all privileges
  if ($user->uid == 1) {
    return true;
  }

  //If the user has access to Administrate Students, allow all operations
  if (user_access(PERM_ADMINISTER_STUDENT, $account)) {
    return true;
  }

  if ($op == 'list') {
    return user_access(PERM_LIST_STUDENT, $account);
  }

  if ($op == 'create') {
    return user_access(PERM_CREATE_STUDENT, $account);
  }

  if ($op == 'edit') {
    return user_access(PERM_EDIT_STUDENT, $account);
  }

  if ($op == 'view') {
    return user_access(PERM_VIEW_STUDENT, $account);
  }

  if ($op == 'delete') {
    return user_access(PERM_DELETE_STUDENT, $account);
  }

  return false;

}

/**
 * Implements hook_permission().
 * This hook is used to define permissions for the module student.
 * All the permissions defined here will be shown on Drupal Backend.
 * These permission will be checked in the hook_access() function.
 */
function student_permission()
{
  return [
    PERM_ADMINISTER_STUDENT => [
      'title' => t('Administer Students'),
      'restrict access' => true,
    ],
    PERM_LIST_STUDENT => [
      'title' => t('List Students'),
      'restrict access' => true,
    ],
    PERM_CREATE_STUDENT => [
      'title' => t('Create Students'),
      'restrict access' => true,
    ],
    PERM_VIEW_STUDENT => [
      'title' => t('View Students'),
      'restrict access' => true,
    ],
    PERM_EDIT_STUDENT => [
      'title' => t('Edit Students'),
      'restrict access' => true,
    ],
    PERM_DELETE_STUDENT => [
      'title' => t('Delete Students'),
      'restrict access' => true,
    ],
  ];
}