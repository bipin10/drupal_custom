<div class="student-data">

  <hr>
  <?php if (empty($student)) {
        drupal_set_message(t('Student data does not exists !!!'), 'error');
        return false;
    } ?>
  <h2>
    <?php echo 'Welcome ' . '<i>'. $student->firstname . ' ' . $student->lastname. '</i>'; ?>
  </h2>

  <h2>
    <?php echo 'Congratulations! You have successfully completed your training session'; ?>
  </h2>

  <h2>
    <?php echo 'Your Roll Number is '. $student->rollnumber; ?>
  </h2>

</div>