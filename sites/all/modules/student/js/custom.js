// (function (Drupal) {
//     Drupal.ajax.prototype.commands.redirect = function (ajax, response) {
//         window.location.reload(response.redirect_url);
//         // alert('here');
//     }
// }(Drupal));

(function ($) {
    // Drupal.behaviors.myBehavior = {
    //     attach: function (context, settings) {

    //         //code starts
    //         $("#edit-image-upload").change(function () {
    //             $("#edit-image-upload-button").trigger('click');
    //             // jQuery('#edit-image-upload-button').click();
    //             // alert($("#edit-image-upload-button").val());
    //             return false;
    //         });
    //         //code ends

    //     }
    // };

})(jQuery);

/***
 * Auto Upload Image
 */
(function ($) {
    Drupal.behaviors.autoUpload = {
        attach: function (context, settings) {
            $('form', context).delegate('input.form-file', 'change', function () {
                $(this).next('input[type="submit"]').mousedown();
            });
        }
    };
})(jQuery);