<?php

/**
 *
 * entity: itonics_segment_tr3
 *
 */

/**
 *
 * Form: Add/Edit form for itonics_segment_tr3
 *
 */
function itonics_segment_tr3_form($form, &$form_state, $segment = null, $type = 1, $is_sub = false)
{

    if (empty($segment)) {
        _check_segment_type($type);
        $form_state['segment_type'] = $type;
        $form_state['is_sub'] = $is_sub;
        $itonics_segment_tr3 = itonics_segment_tr3_new('itonics_segment_tr3');

        $segment_title = _get_segment_title($type);

        itonics_entity_details_set_breadcrumb(array(t('Start') => '<front>', t('Explorer') => 'innovation-wall/all', $segment_title => '#'));
    } else {
        $form['psid'] = array(
            '#type' => 'hidden',
            '#default_value' => $segment->psid,
        );

        $form_state['segment_type'] = $segment->seg_type;

        $type = $segment->seg_type;

        $is_sub = ($segment->seg_parent > 0) ? true : false;

        $form_state['is_sub'] = $is_sub;

        $segment_title = _get_segment_title($segment->seg_type);

        drupal_set_title($segment_title . ': ' . html_entity_decode($segment->title_en));
        $itonics_segment_tr3 = $segment;
        itonics_entity_details_set_breadcrumb(array(t('Start') => '<front>', t('Explorer') => 'innovation-wall/all', $segment_title . ': ' . $itonics_segment_tr3->title_en => 'segment/' . $itonics_segment_tr3->psid . '/details'));
    }

    $options = array();
    drupal_add_js(drupal_get_path('module', 'itonics_segment_tr3') . '/js/tabbedContent.js', $options);

    $weight = 0;

    // $form['segment_form_header'] = array(
    //     '#theme' => 'itonics_segment_tr3_form_header_view',
    //     '#weight' => $weight++,
    // );

    $form['title_en'] = array(
        '#type' => 'textfield',
        '#id' => 'segment_title_en',
        '#prefix' => '<ul class="slideshow-content clearfix active" id="sc_1"><li>
            <div class="element-tab-wrapper form-elements"><div class="form-element-row-wrapper"><div class="title-input-wrapper left"><div class="title-wrapper">' . t("Title") . " EN <span class='required-field'> * </span></div>",
        '#suffix' => '<div class="validation-error-msg" id="segment-title_en-error-msg"></div></div>',
        '#required' => true,
        '#default_value' => isset($itonics_segment_tr3->title_en) ? html_entity_decode($itonics_segment_tr3->title_en) : '',
        '#weight' => $weight++,
    );

    $form['title_de'] = array(
        '#type' => 'textfield',
        '#id' => 'segment_title_de',
        '#prefix' => '<ul class="slideshow-content clearfix active" id="sc_1"><li>
            <div class="element-tab-wrapper form-elements"><div class="form-element-row-wrapper"><div class="title-input-wrapper left"><div class="title-wrapper">' . t("Title") . " DE <span class='required-field'> * </span></div>",
        '#suffix' => '<div class="validation-error-msg" id="segment-title_de-error-msg"></div></div>',
        '#required' => true,
        '#default_value' => isset($itonics_segment_tr3->title_de) ? html_entity_decode($itonics_segment_tr3->title_de) : '',
        '#weight' => $weight++,
    );

    $form['abstract_en'] = array(
        '#type' => 'textarea',
        '#id' => 'segment_abstract_en',
        '#prefix' => '<div class="title-input-wrapper"><div class="title-wrapper">' . t("Abstract") . " <span class='required-field'> * </span></div>",
        '#suffix' => '<div class="validation-error-msg" id="segment-abstract_en-error-msg"></div></div>',
        '#required' => true,
        '#default_value' => isset($itonics_segment_tr3->abstract_en) ? $itonics_segment_tr3->abstract_en : '',
        '#weight' => $weight++,
        '#rows' => 5,
        '#cols' => 10,
    );

    if ($is_sub) {

        $parent_segments = entity_get_controller(ITONICS_SEGMENT_TR3_ENTITY)->get_all_segments($type);
        $parent_segments[0] = t('Select');
        ksort($parent_segments);

        if ($type == 1) {
            $first_level_title = t('Parent Segment');
        } elseif ($type == 3) {
            $first_level_title = t('Merchandise Group');
        }

        $form['seg_parent'] = array(
            '#type' => 'select',
            '#options' => $parent_segments,
            '#id' => 'segment_seg_parent',
            '#prefix' => '<div class="form-element-row-wrapper"><div class="title-input-wrapper left"><div class="title-wrapper">' . $first_level_title . " <span class='required-field'> * </span></div>",
            '#suffix' => '<div class="validation-error-msg" id="segment-seg_parent-error-msg"></div></div></div>',
            '#required' => true,
            '#default_value' => isset($itonics_segment_tr3->seg_parent) ? $itonics_segment_tr3->seg_parent : '',
            '#weight' => $weight++,
        );
    }

    if (!$is_sub && $type == 3) {
        $parent_segments = entity_get_controller(ITONICS_SEGMENT_TR3_ENTITY)->get_all_fields(4);
        $parent_segments[0] = t('Select');
        ksort($parent_segments);

        $form['parent_field'] = array(
            '#type' => 'select',
            '#options' => $parent_segments,
            '#id' => 'parent_field',
            '#prefix' => '<div class="form-element-row-wrapper"><div class="title-input-wrapper left"><div class="title-wrapper">' . t("Merchandise Line") . " <span class='required-field'> * </span></div>",
            '#suffix' => '<div class="validation-error-msg" id="segment-seg_parent-error-msg"></div></div></div>',
            '#required' => true,
            '#default_value' => isset($itonics_segment_tr3->parent_field) ? $itonics_segment_tr3->parent_field : '',
            '#weight' => $weight++,
        );
    }

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['#prefix'] = "<div class='control-wrapper'>";
    $form['actions']['#suffix'] = "</div>";
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#id' => 'form-segment',
        '#value' => t('Save'),
        '#prefix' => '<div id="controls">',
        '#suffix' => '</div>',
        '#weight' => $weight++,
        '#ajax' => array(
            'callback' => 'itonics_segment_add_form_ajax_submit',
            'wrapper' => '',
            'event' => 'click'
        ),
    );

    // This function clicks on GENERAL Tab when error is encountered on the form while saving.
    drupal_add_js('Drupal.ajax.prototype.commands.loadDefaultMrd = function(ajax , response , status){
      jQuery("#mrd_1").click();
  }', 'inline');


    return $form;
}

function itonics_segment_tr3_form_validate($form, &$form_state)
{

    //xss protection
    $whiteList = array('description_en', 'description_en');
    itonics_filter_xss_all($form_state['values'], $whiteList);
    itonics_filter_xss_all($form_state['input'], $whiteList);
}

/**
 * Submit: itonics_segment_tr3_form
 */
function itonics_segment_add_form_ajax_submit($form, &$form_state)
{
    try {
        $title_en = trim(check_plain($form_state['values']['title_en']));
        $title_de = trim(check_plain($form_state['values']['title_de']));

        $abstract_en = $form_state['values']['abstract_en'];

        if ($title_en == "") {
            form_set_error('title_en');
        }

        if ($title_de == "") {
            form_set_error('title_de');
        }

        if (strlen($title_en) > 255) {
            form_set_error('title_en', t('Maximum length of title is 255 characters.'));
        }

        if (strlen($title_de) > 255) {
            form_set_error('title_de', t('Maximum length of title is 255 characters.'));
        }

        $is_sub = $form_state['is_sub'];
        if ($is_sub) {
            if ($form_state['values']['seg_parent'] == 0) {
                form_set_error('seg_parent');
            }
        }

        $error = form_get_errors();

        $commands = array();
        $required_fields = array('title_en', 'title_de', 'abstract_en', 'seg_parent');
        if ($error) {
            $commands[] = array("command" => 'loadDefaultMrd');
            //Looping through the errors to add error class and error message
            foreach ($required_fields as $row) {
                if (isset($error[$row])) {
                    $commands[] = ajax_command_invoke('#segment_' . $row, 'addClass', array('error'));
                    $commands[] = ajax_command_html('#segment-' . $row . '-error-msg', t('This field is required'));
                } else {
                    $commands[] = ajax_command_invoke('#segment_' . $row, 'removeClass', array('error'));
                    $commands[] = ajax_command_html('#segment-' . $row . '-error-msg', '');
                }
            }
        } else {
            global $user;

            $segment = (object)$form_state['values'];

            $segment->title_de = $form_state['values']['title_de'];
            $segment->abstract_de = $form_state['values']['abstract_en'];
            $segment->description_en = $segment->description_en['value'];
            $segment->description_de = $segment->description_en;

            $segment->seg_type = intval($form_state['segment_type']);

            if (empty($form_state['values']['psid'])) {
                $segment->weight = get_weight(intval($form_state['segment_type']));
            }
            if (isset($form_state['values']['parent_field'])) {
                $segment->parent_field = $form_state['values']['parent_field'];
            }
            // Notify field widgets.
            field_attach_submit('itonics_segment_tr3', $segment, $form, $form_state);
            // Save the itonics_segment_tr3.
            segment_new($segment);

            // Notify the user.
            drupal_set_message(t('Segment saved.'));
            global $language;
            //$redirect_url = $language->language .'/segment/' . $segment->psid . '/details';
            $segment_type = $form_state['segment_type'];
            if ($is_sub) {
                $redirect_url = 'manage/sub-segment/' . $segment_type;
//                $redirect_url = $language->language . '/manage/sub-segment/' . $segment_type;
            } else {
                $redirect_url = 'manage/segment/' . $segment_type;
//                $redirect_url = $language->language . '/manage/segment/' . $segment_type;
            }

            $commands[] = ajax_command_invoke(null, 'redirect', array($redirect_url));
        }
    } catch (Exception $e) {
        drupal_set_message(t('Some thing went wrong! Try again later!'), 'error');
    }

    print ajax_render($commands);
    drupal_exit();
}

/*
 * Confirmation dialog box for deleting a segment
 */

function itonics_segment_tr3_delete_confirm($form, &$form_state, $segment)
{
    $seg = $segment;

    $segment_title = _get_segment_title($segment->seg_type);

    itonics_entity_details_set_breadcrumb(array(t('Start') => '<front>', t('Explorer') => 'innovation-wall/all', $segment_title . ': ' . $segment->title_en => 'segment/' . $segment->psid . '/details'));

    $form['#itonics_segment_tr3'] = $seg;
    $form['psid'] = array('#type' => 'value', '#value' => $seg->psid);
    return confirm_form($form, t('Are you sure you want to delete %title ?', array('%title' => html_entity_decode($seg->title_en))), 'segment/' . $seg->psid . '/details', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Delete data are submitted here and deleted
 * @param <type> $form
 * @param array $form_state
 */
function itonics_segment_tr3_delete_confirm_submit($form, &$form_state)
{
    global $base_url;
    if ($form_state['values']['confirm']) {
        $itonics_segment_tr3 = itonics_segment_tr3_load($form_state['values']['psid']);
        itonics_segment_tr3_delete($form_state['values']['psid']);
        watchdog('itonics_segment_tr3', 'Segment: <em> %title </em> is deleted .', array('%title' => $itonics_segment_tr3->title_en));
        drupal_set_message(t('Segment %title has been deleted.', array('%title' => html_entity_decode($itonics_segment_tr3->title_en))));
    }
    $form_state['redirect'] = '<front>';
}

/**
 * gets the weight
 * @param type $segment
 */
function get_weight($segment)
{
    $query = db_select('itonics_segment_tr3');
    $query->addExpression('MAX(weight) + 1');
    $query->condition('seg_type', $segment);
    $max = $query->execute()->fetchField();
    return $max;
}
