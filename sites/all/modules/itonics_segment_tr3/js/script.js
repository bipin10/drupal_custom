(function ($) {



    $(document).ready(function () {

        var secondlevels = Drupal.settings.megatrend.secondlevel;

        var firstarr = Drupal.settings.megatrend.firstarr;

        asel = Drupal.settings.megatrend.selentity_segment;
        bsel = Drupal.settings.megatrend.selentity_sub_segment;

        // if parent is by default selected then get its child list
        if (parseInt(asel) > 0) {
            if (typeof secondlevels !== 'undefined') {
                hierarchysecond(parseInt(asel));
            }
        }

        function hierarchysecond(parent) {
            // If PARENT is EMPTY , Then RESET CHILD and SUB CHILD option values
            if (parent == '') {
                $("#entity_sub_segment").html(""); //reset child options
                $('#entity_sub_segment').multiselect('destroy');  // tell widget to clear itself
                $("#entity_sub_segment").append("<option value=''>" + Drupal.t('Select options') + "</option>");

                for (i in secondlevels) {
                    childlist(secondlevels[i]);
                }

                $('#entity_sub_segment').multiselect({
                    header: false,
                    selectedList: 5,
                    multiple: false,
                    height: '200',
                    noneSelectedText: Drupal.t('Select options'),
                }); // re-initialize the widget

            } else {

                if (typeof secondlevels !== 'undefined') {
                    fillsecondlist(secondlevels[parent], bsel);
                }
            }
        }


        //If parent option is changed
        $("#entity_segment").change(function () {
            var parent = $(this).val(); //get option value from parent
            hierarchysecond(parent);
        });

        function fillsecondlist(array_list, selid)
        {

            $("#entity_sub_segment").html(""); //reset child options
            $('#entity_sub_segment').multiselect('destroy');  // tell widget to clear itself

            $(array_list).each(function (i) { //populate child options
                var issel = '';
                if (typeof selid !== 'undefined') {
                    issel = (parseInt(selid) === parseInt(array_list[i].value)) ? 'selected' : '';
                }

                $("#entity_sub_segment").append("<option " + issel + " value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>");
            });

            var heightVal = (array_list.length < 10) ? 'auto' : '200';

            $('#entity_sub_segment').multiselect({
                header: false,
                selectedList: 5,
                multiple: false,
                height: heightVal,
                noneSelectedText: Drupal.t('Select options'),
            }); // re-initialize the widget
        }



        $("#entity_sub_segment").change(function () {

            var selectedval = $(this).val(); //get option value of hierachy second

            if ($('#entity_segment').val() == '') {

                if (typeof firstarr !== 'undefined') {

                    // STEP 1:  As Parent not selected, Make parent selected first
                    parentId = firstarr[selectedval];
                    $('#entity_segment').val(parentId)

                    $("#entity_segment").multiselect({
                        header: false,
                        selectedList: 10,
                        multiple: false,
                        height: 'auto',
                        noneSelectedText: Drupal.t('Select options'),
                    });

                    // Reset child option according to parentid See: Line 134
                    if (typeof secondlevels !== 'undefined') {
                        fillsecondlist(secondlevels[parentId], selectedval);
                    }

                    //$("#entity_sub_segment").trigger("change");
                }

            }

        });


        function childlist(array_list)
        {
            $(array_list).each(function (i) { //populate child options
                if (array_list[i].value != '') {
                    $("#entity_sub_segment").append("<option value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>");
                }
            });
        }

    });

    Drupal.behaviors.trendFrom = {
        attach: function (context, settings) {

            if ($.isFunction($.fn.multiselect)) {

                $("#entity_segment").multiselect({
                    header: false,
                    selectedList: 10,
                    multiple: false,
                    height: 'auto',
                    noneSelectedText: Drupal.t('Select options'),
                });

                $("#entity_sub_segment").multiselect({
                    header: false,
                    selectedList: 10,
                    multiple: false,
                    height: 'auto',
                    noneSelectedText: Drupal.t('Select options'),
                });

            }

        },
        detach: function (context, settings) {

        }
    };

}(jQuery));
