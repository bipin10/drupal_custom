(function ($) {



    $(document).ready(function () {

        var secondlevels = Drupal.settings.megatrend.secondlevel;
        var thirdlevels = Drupal.settings.megatrend.thirdlevel;

        var firstarr = Drupal.settings.megatrend.firstarr;
        var secondarr = Drupal.settings.megatrend.secondarr;

        asel = Drupal.settings.megatrend.technology_fields;
        bsel = Drupal.settings.megatrend.selentity_segment;
        lastfield = Drupal.settings.megatrend.selentity_sub_segment;
        // console.log(secondlevels);
        // if parent is by default selected then get its child list
        if (parseInt(asel) > 0) {
            if (typeof secondlevels !== 'undefined') {
                hierarchysecond(parseInt(asel));
            }
        }
        if (parseInt(bsel) > 0) {
            if (typeof thirdlevels !== 'undefined') {
                hierarchythird(parseInt(bsel));
            }
        }

        function hierarchysecond(parent) {
            // If PARENT is EMPTY , Then RESET CHILD and SUB CHILD option values
            if (parent == '') {
                $("#entity_segment").html(""); //reset child options
                $('#entity_segment').multiselect('destroy');  // tell widget to clear itself
                $("#entity_segment").append("<option value=''>" + Drupal.t('Select options') + "</option>");

                for (i in secondlevels) {
                    childlist(secondlevels[i]);
                }

                $('#entity_segment').multiselect({
                    header: false,
                    selectedList: 5,
                    multiple: false,
                    height: '200',
                    noneSelectedText: Drupal.t('Select options'),
                }); // re-initialize the widget

            } else {

                if (typeof secondlevels !== 'undefined') {
                    fillsecondlist(secondlevels[parent], bsel);
                }
            }
        }
        function hierarchythird(parent) {
            // If PARENT is EMPTY , Then RESET CHILD and SUB CHILD option values
            if (parent == '') {
                $("#entity_sub_segment").html(""); //reset child options
                $('#entity_sub_segment').multiselect('destroy');  // tell widget to clear itself
                $("#entity_sub_segment").append("<option value=''>" + Drupal.t('Select options') + "</option>");

                for (i in thirdlevels) {
                    childlist1(thirdlevels[i]);
                }

                $('#entity_sub_segment').multiselect({
                    header: false,
                    selectedList: 5,
                    multiple: false,
                    height: '200',
                    noneSelectedText: Drupal.t('Select options'),
                }); // re-initialize the widget

            } else {

                if (typeof thirdlevels !== 'undefined') {
                    fillthirdlist(thirdlevels[parent], lastfield);
                }
            }
        }

        //If parent option is changed
        $("#entity_field").change(function () {
            $('#entity_sub_segment option').prop('selected', function() {
                return this.defaultSelected;
            });
            var parent = $(this).val(); //get option value from parent
            hierarchysecond(parent);
        });
 /*       //If parent option is changed
        $("#entity_segment").change(function () {
            var parent = $(this).val(); //get option value from parent
            hierarchythird(parent);
        });*/

        function fillsecondlist(array_list, selid)
        {

            $("#entity_segment").html(""); //reset child options
            $('#entity_segment').multiselect('destroy');  // tell widget to clear itself

            $(array_list).each(function (i) { //populate child options
                var issel = '';
                if (typeof selid !== 'undefined') {
                    issel = (parseInt(selid) === parseInt(array_list[i].value)) ? 'selected' : '';
                }

                $("#entity_segment").append("<option " + issel + " value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>");
            });

            var heightVal = (array_list.length < 10) ? 'auto' : '200';

            $('#entity_segment').multiselect({
                header: false,
                selectedList: 5,
                multiple: false,
                height: heightVal,
                noneSelectedText: Drupal.t('Select options'),
            }); // re-initialize the widget
        }
        function fillthirdlist(array_list, selid)
        {

            $("#entity_sub_segment").html(""); //reset child options
            $('#entity_sub_segment').multiselect('destroy');  // tell widget to clear itself

            $(array_list).each(function (i) { //populate child options
                var issel = '';
                if (typeof selid !== 'undefined') {
                    issel = (parseInt(selid) === parseInt(array_list[i].value)) ? 'selected' : '';
                }

                $("#entity_sub_segment").append("<option " + issel + " value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>");
            });

            var heightVal = (array_list.length < 10) ? 'auto' : '200';

            $('#entity_sub_segment').multiselect({
                header: false,
                selectedList: 5,
                multiple: false,
                height: heightVal,
                noneSelectedText: Drupal.t('Select options'),
            }); // re-initialize the widget
        }


        $("#entity_segment").change(function () {

            var selectedval = $(this).val(); //get option value of hierachy second

            if ($('#entity_field').val() == '') {

                if (typeof firstarr !== 'undefined') {

                    // STEP 1:  As Parent not selected, Make parent selected first
                    parentId = firstarr[selectedval];
                    $('#entity_field').val(parentId)

                    $("#entity_field").multiselect({
                        header: false,
                        selectedList: 10,
                        multiple: false,
                        height: 'auto',
                        noneSelectedText: Drupal.t('Select options'),
                    });

                    // Reset child option according to parentid See: Line 134
                    if (typeof secondlevels !== 'undefined') {
                        fillsecondlist(secondlevels[parentId], selectedval);
                    }

                    //$("#entity_sub_segment").trigger("change");
                }

            }else{
                var parent = $(this).val(); //get option value from parent
                hierarchythird(parent);
            }

        });


        function childlist(array_list)
        {
            $(array_list).each(function (i) { //populate child options
                if (array_list[i].value != '') {
                    $("#entity_segment").append("<option value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>");
                }
            });
        }
        function childlist1(array_list)
        {
            $(array_list).each(function (i) { //populate child options
                if (array_list[i].value != '') {
                    $("#entity_sub_segment").append("<option value=\"" + array_list[i].value + "\">" + array_list[i].display + "</option>");
                }
            });
        }

    });

    Drupal.behaviors.trendFrom = {
        attach: function (context, settings) {

            if ($.isFunction($.fn.multiselect)) {

                $("#entity_segment").multiselect({
                    header: false,
                    selectedList: 10,
                    multiple: false,
                    height: 'auto',
                    noneSelectedText: Drupal.t('Select options'),
                });

                $("#entity_sub_segment").multiselect({
                    header: false,
                    selectedList: 10,
                    multiple: false,
                    height: 'auto',
                    noneSelectedText: Drupal.t('Select options'),
                });
                $("#entity_field").multiselect({
                    header: false,
                    selectedList: 10,
                    multiple: false,
                    height: 'auto',
                    noneSelectedText: Drupal.t('Select options'),
                });
            }

        },
        detach: function (context, settings) {

        }
    };

}(jQuery));
