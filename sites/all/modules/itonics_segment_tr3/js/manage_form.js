(function($) {
	
	Drupal.behaviors.buManage = {
		attach : function(context, settings) {
			if($('.bu-manage-form.confirmation').length == 1) {
				$('ul.action-links').hide();
			}
			if(Drupal.tableDrag && Drupal.tableDrag['bu_reorder']){
				$('div.tabledrag-toggle-weight-wrapper').remove();
				Drupal.tableDrag['bu_reorder'].onDrop = function(){ 
					var dragObject = this;
					$(dragObject.rowObject.element).find('span.tabledrag-changed').remove();
					$('div.tabledrag-changed-warning').remove();
					//get all the weights of all elements
					var weights = {};
					$('.position-weight').each(function(){
						weights[parseInt($(this).attr('psid'))] = parseInt($(this).val());
					});
					
					//console.log(weights);
					jQuery('.loader').show();
					//save weights via ajax
					$.ajax( {
	                    url : Drupal.settings.basePath + "manage/segment/saveweight",
	                    type : "POST",
	                    dataType : "json",
	                    data : {'weights':weights},
	                    success : function() {
	                        jQuery('.loader').hide();
	                    },
	                    error : function() {
	                        alert(Drupal.t("Some error occured, Please try again later."));
	                    }
	                });
				};
			}
			
			$('input.checkall').click(function(){
				var checked = $(this).prop('checked');
				$('input.bu-select').prop('checked', checked);
			});

		},
		
		detach : function(context, settings) {
			
		}
	};

}(jQuery));