(function ($) {
    var TabbedContent = {
        init: function () {
            var newWidth = $(".tab_item.active").width();
            jQuery(".moving_bg").width(newWidth);

            $(".tab_item").click(function () {
                $("#create-element-tab span.tab_item").removeClass("active");
                $("#slideshow-holder ul.slideshow-content").removeClass("active");

                $(this).addClass("active");
                var newWidth = $(this).width();
                var background = $(this).parent().find(".moving_bg");

                $(background).stop().animate({
                    width: $(this).width(),
                    left: $(this).position()['left']

                }, {
                    duration: 300
                });

                TabbedContent.slideContent($(this));
                jQuery('.mceLayout').css({'height': '335px', 'width': '99%'});
                jQuery('.mceLayout iframe').css({'height': '335px', 'width': '99%'});
               // update_nav_visibility();

            });
        },
        slideContent: function (obj) {

            var margin = $(obj).parent().parent().find(".slide_content").width();
            margin = margin * ($(obj).prevAll().size() - 1);
            margin = margin * -1;

            $(obj).parent().parent().find(".tabslider").stop().animate({
                marginLeft: margin + "px"
            }, {
                duration: 300
            });
        }
    }


    $(document).ready(function () {

        var visibleradio = $('#visibility-wrapper a[href] , #state-wrapper a[href]');

        $(visibleradio).qtip({
            content: {
                text: false // Use each elements title attribute
            },
            style: {
                tip: true, // Apply a speech bubble tip to the tooltip at the designated tooltip corner
                border: {
                    width: 0,
                    radius: 4
                },
                name: 'light', // Use the default light style
                width: 400
            },
            position: {
                corner: {
                    target: 'topRight', // Position the tooltip above the link
                    tooltip: 'bottomMiddle'
                },
            },
        });

        //Hide the visibility options when page loads, since the status will be Draft
        jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper').hide();

        // When status is set to Hand-In, Show the visibility status, hide the draft visibility status and set the second radio button to checked(i.e Itonics Team)
        jQuery('.page-idea .element-tab-left-wrapper #state-wrapper input#edit-state-2').click(function (e) {
            jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper').show();
            jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper #edit-visibility .visibility-radio-wrapper:first').hide();
            jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper input#edit-visibility-2').prop('checked', true);
        });

        // When status is set to Draft, Hide the visibility status and set the first radio button to checked(i.e. Draft)
        jQuery('.page-idea .element-tab-left-wrapper #state-wrapper input#edit-state-1').click(function (e) {
            jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper #edit-visibility .visibility-radio-wrapper:first').show();
            jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper').hide();
            jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper input#edit-visibility-1').prop('checked', true);
        });

        /**
         jQuery("#idea-details-wrapper,#all-interlinkage-wrapper").slimScroll({
         height:"420px", 
         railVisible: false,
         alwaysVisible: false
         });
         **/
        TabbedContent.init();
        //update_nav_visibility();
        $('#slideshow-previous').click(function () {
            rotate_slider('prev');
        });

        $('#slideshow-next').click(function () {
            rotate_slider('next');
        });


        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            //addding swipe right effect to slide in the description bar for elements in radar page.
            $("#slideshow-scroller").touchwipe({
                wipeLeft: function () {
                    if ($('#slideshow-next').is(":visible"))
                    {
                        rotate_slider('next');
                    }

                },
                wipeRight: function () {
                    if ($('#slideshow-previous').is(":visible"))
                    {
                        rotate_slider('prev');
                    }
                },
                preventDefaultEvents: true
            });
        }

        jQuery('body').on('keypress', '#itonics-idea-tr3-form input', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }
        });

//        jQuery('#sitewrapper').mouseup(function () {
//            update_nav_visibility();
//        });
//
//        jQuery('.right-column-content').mouseup(function () {
//            update_nav_visibility();
//        });
        
//        jQuery("#source").multiselect({
//            selectedList: 2,
//            header: false,
//            height: 'auto',
//        });
    });
    //Increase height when add more.........
//    Drupal.behaviors.itonics_technology_tr3 = {
//        attach: function (context, settings) {
//            update_nav_visibility();
//        },
//        detach: function (context, settings) {
//        }
//    };

}(jQuery));
//
jQuery(window).load(function (e) {
    //If default checked option is Hand-In, show the visibility Tab'
    if (jQuery('.page-idea .element-tab-left-wrapper #state-wrapper input#edit-state-2').prop("checked")) {
        jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper').show();
        jQuery('.page-idea .element-tab-visibility-wrapper #visibility-wrapper #edit-visibility .visibility-radio-wrapper:first').hide();
    }
});