<?php

/**
 * @file
 * Provides a central controller for status entity.
 *
 */
class SegmentController extends DrupalDefaultEntityController {

    /**
     * Create a default status.
     *
     * @return
     *   A status object with all default fields initialized.
     */
    public function create() {
        return (object) array(
                    'psid' => '',
                    'title_en' => '',
                    'title_de' => '',
                    'description_en' => '',
                    'description_de' => '',
                    'uid' => '',
                    'weight' => '',
                    'type' => ITONICS_SEGMENT_TR3_ENTITY,
        );
    }

    /**
     * Permanently saves the given entity.
     *
     * In case of failures, an exception is thrown.
     *
     * @param $itonics_segment_tr3
     *   The entity to save.
     *
     * @return
     *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
     */
    public function save($pestle) {
        $transaction = db_transaction();
        try {
            global $user;
            $pestle->is_new = empty($pestle->psid);

            $pestle->revision_timestamp = REQUEST_TIME;

            // Giving modules the opportunity to prepare field data for saving.
            field_attach_presave(ITONICS_SEGMENT_TR3_ENTITY, $pestle);

            if ($pestle->is_new) {
                $pestle->uid = $user->uid;
                // Setting the timestamp fields.
                $pestle->created = REQUEST_TIME;
                $pestle->changed = REQUEST_TIME;
                drupal_write_record(ITONICS_SEGMENT_TR3_BASE_TABLE, $pestle);
                $op = 'insert';
            } else {
                $pestle->changed = REQUEST_TIME;
                // Saving the updated status.
                drupal_write_record(ITONICS_SEGMENT_TR3_BASE_TABLE, $pestle, 'psid');
                $op = 'update';
            }
            // Saving fields.
            $function = 'field_attach_' . $op;
            $function(ITONICS_SEGMENT_TR3_ENTITY, $pestle);
            module_invoke_all('entity_' . $op, $pestle, ITONICS_SEGMENT_TR3_BASE_TABLE);
            // Clearing internal properties.
            unset($pestle->is_new);
            // Ignoring slave server temporarily to give time for the saved order to be propagated to the slave.
            db_ignore_slave();
            return $pestle;
        } catch (Exception $e) {
            $transaction->rollback();
            watchdog_exception(ITONICS_SEGMENT_TR3_ENTITY, $e, NULL, WATCHDOG_ERROR);
            return FALSE;
        }
    }

    /**
     * Delete permanently saved entities.
     *
     * In case of failures, an exception is thrown.
     *
     * @param $buids
     *   An array of entity IDs.
     *
     */
    public function delete($psids) {
        if (!empty($psids)) {
            $statuses = $this->load($psids, array());
            $transaction = db_transaction();
            try {
                db_delete(ITONICS_SEGMENT_TR3_BASE_TABLE)
                        ->condition('psid', $psids, 'IN')
                        ->execute();
                foreach ($statuses as $status_id => $status) {
                    field_attach_delete(ITONICS_SEGMENT_TR3_BASE_TABLE, $status);
                }
                db_ignore_slave();
            } catch (Exception $e) {
                $transaction->rollback();
                watchdog_exception(ITONICS_SEGMENT_TR3_BASE_TABLE, $e, NULL, WATCHDOG_ERROR);
                return FALSE;
            }
            module_invoke_all('entity_delete', $status, ITONICS_SEGMENT_TR3_BASE_TABLE);
            // Clearing the page and block and status caches.
            cache_clear_all();
            $this->resetCache();
        }
        return TRUE;
    }

    /**
     * Get all Itonics  Segment
     *
     */
    public function get_all_segments($type = 1, $parent_segment = 0) {
        $segment = array();
        $title = $this->get_segment_titlefieldname();

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle')
                ->condition('pestle.seg_type', $type)
                ->fields('pestle', array('psid', $title))
                ->orderBy('pestle.weight', 'ASC');


        $query->condition('pestle.seg_parent', $parent_segment);

        $results = $query->execute()->fetchAll();
        //echo '<pre>';print_r($results);exit;
//        while ($results = $result->fetchObject()) {
//            $segment[$results->psid] = $results->$title;
//        }
        foreach ($results as $key => $result):
            $segment[$result->psid] = $result->$title;
        endforeach;

        return $segment;
    }
    public function get_all_fields($type = 4, $parent_segment = 0) {
        $segment = array();
        $title = $this->get_segment_titlefieldname();

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle')
            ->condition('pestle.seg_type', $type)
            ->fields('pestle', array('psid', $title))
            ->orderBy('pestle.weight', 'ASC');


        $query->condition('pestle.seg_parent', $parent_segment);

        $results = $query->execute()->fetchAll();
        //echo '<pre>';print_r($results);exit;
//        while ($results = $result->fetchObject()) {
//            $segment[$results->psid] = $results->$title;
//        }
        foreach ($results as $key => $result):
            $segment[$result->psid] = $result->$title;
        endforeach;

        return $segment;
    }

    /**
     * Get all Itonics  Segment
     *
     */
    public function get_segment_options($type = 1, $seg_parent = 0) {
        $segment = array();
        //$title = $this->get_segment_titlefieldname();

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle')
                ->condition('pestle.seg_type', $type)
                ->condition('pestle.seg_parent', $seg_parent)
                ->fields('pestle', array('psid', 'title_en'));
        $segments = $query->execute()->fetchAll();

        return $segments;
    }

    /**
     * Function to get all parent
     *
     * @param array $segids
     * @param int $status
     * @param $orderby
     * @param $order
     *
     * @return mixed
     */
    public function get_segment_expert_rels($segids = array()) {

        $query = db_select('itonics_segment_tr3', 'seg');
        $query->fields('seg', array('psid', 'title_en', 'seg_parent'));
        $query->condition('seg_parent', $segids, 'IN');
        $entity_sub_segments = $query->execute()->fetchAll();
        return $entity_sub_segments;
    }
    public function get_field_expert_rels($segids = array()) {

        $query = db_select('itonics_segment_tr3', 'seg');
        $query->fields('seg', array('psid', 'title_en', 'seg_parent','parent_field'));
        $query->condition('parent_field', $segids, 'IN');
        $entity_sub_segments = $query->execute()->fetchAll();
        return $entity_sub_segments;
    }

    /**
     * Get all Itonics  Segment titles only
     *
     */
    public function get_all_title($type = 1) {
        $segment = array();
        $segment[0] = t("Select options");
        $title = $this->get_segment_titlefieldname();

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle');
        $query->fields('pestle', array('psid'));
        $query->addField('pestle', $title, 'title_de');
        $query->condition('pestle.seg_type', $type);
        $query->orderBy('pestle.title_de');
        $result = $query->execute();
        while ($results = $result->fetchObject()) {
            $segment[$results->psid] = $results->title_de;
        }
        return $segment;
    }

    /**
     * Get all Itonics Pestle SEgment english titles only
     *
     */
    public function get_en_title($psid) {
        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle')
                ->fields('pestle', array('psid', 'title_de'))
                ->condition('psid', $psid, '=');
        $result = $query->execute();
        $res = $result->fetch();

        return $res->title_de;
    }

    /**
     * Get all Itonics  SEgment id's only
     *
     * @param $sort
     *   Optional sort
     */
    public function get_all_ids($sort = NULL, $type = 1) {
        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle')
                ->condition('pestle.seg_type', $type)
                ->fields('pestle', array('psid'));
        if ($sort) {
            $query->orderBy($sort);
        }

        $result = $query->execute();

        $results = array();
        foreach ($result as $row) {
            $results[] = $row->psid;
        }

        return $results;
    }

    /**
     * Load a  Segment
     *
     * @param type $name
     */
    public function itonics_segment_tr3_load($psid) {

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle')
                ->fields('pestle');
        $query->distinct();
        $result = $query->execute()->fetchObject();
        if ($result) {
            return $result;
        } else {
            // no result
            return FALSE;
        }
    }

    /**
     *
     * Search  Segment
     *
     */
    public function search_segment($search = '') {
        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'i');
        $query->fields('i');

        $or = db_or();
        $or->condition('i.title_en', '%' . db_like($search) . '%', 'LIKE');
        $or->condition('i.title_de', '%' . db_like($search) . '%', 'LIKE');

        $query->condition($or);
        $query->distinct();

        $result = $query->execute();
        $pestles = array();
        while ($pestle = $result->fetchObject()) {
            $pestles[] = array('iid' => $pestle->psid, 'title_en' => $pestle->title_en, 'title_de' => $pestle->title_de);
        }

        return $pestles;
    }

    public function get_landing_page_segments() {
        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'pestle');
        $query->fields('pestle', array('psid', 'title_en', 'title_de', 'created'));
        $query->orderBy("pestle.psid", "DESC");
        $query->range(0, 3);
        $pestles = $query->execute()->fetchAll();

        $pestles_list = array();
        foreach ($pestles as $key => $val) {
            $val->entity_type = 'segment';
            $pestles_list[$val->psid] = $val;
        }
        return $pestles_list;
    }

    public function get_segment_titlefieldname() {
        global $language;
        if ($language->language == 'en' OR $language->language == 'de') {
            $titleFieldName = 'title_' . $language->language;
        }
        // default to title_en
        else {
            $titleFieldName = 'title_de';
        }
        return $titleFieldName;
    }

    public function get_segment_title($segment) {
        $titleFieldName = $this->get_segment_titlefieldname();

        if (is_numeric($segment)) {
            $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'segment');
            $query->condition('segment.psid', $segment);
            $query->fields('segment', array($titleFieldName));
            $title = $query->execute()->fetchField();
        } else {
            $title = $segment->$titleFieldName;
        }
        return $title;
    }

    /**
     * Get all Itonics  Field Option
     *
     */
    public function get_all_saved_datas($entityid, $entitytype) {

        $query = db_select('itonics_entity_itonics_segment_tr3_rel', 'frel');
        $query->fields('frel');
        $query->condition('frel.entityid', $entityid);
        $query->condition('frel.entitytype', $entitytype);
        return $results = $query->execute()->fetch();
    }

    function getSegmentsOptionsOrder($type = 1, $seg_parent = 0) {
        global $language;

        if ($language->language == 'en' OR $language->language == 'de') {
            $titleFieldName = 'title_' . $language->language;
        }
        // default to title_en
        else {
            $titleFieldName = 'title_de';
        }
        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 's');
        $query->fields('s', array('psid', $titleFieldName, 'weight'));
        $query->condition('s.seg_type', $type);
        $query->condition('s.seg_parent', $seg_parent);
        $query->orderBy('s.weight', 'asc');

        $result = $query->execute()->fetchAll();

        $segments = array();

        foreach ($result as $val) {
            $segments[$val->psid] = $val->{$titleFieldName};
        }
        return $segments;
    }

    function itonics_segment_tr3_get_all_segments_by_weight($type = 1, $seg_parent = 0) {

        global $language;

        if ($language->language == 'en' OR $language->language == 'de') {
            $titleFieldName = 'title_' . $language->language;
        } else {
            $titleFieldName = 'title_de';
        }

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 's');
        $query->fields('s', array('psid', $titleFieldName, 'weight','parent_field'));
        $query->condition('s.seg_type', $type);
        $query->condition('s.seg_parent', $seg_parent);
        $query->orderBy('s.weight', 'ASC');
        $result = $query->execute()->fetchAll();
        $datas = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $datas[$row->psid] = array(
                    'weight' => $row->weight,
                    'title' => $row->{$titleFieldName},
                    'parent_field' => $row->parent_field
                );
            }
        }
        return $datas;
    }

    function itonics_segment_tr3_get_all_fields_by_weight($type = 4, $seg_parent = 0) {

        global $language;

        if ($language->language == 'en' OR $language->language == 'de') {
            $titleFieldName = 'title_' . $language->language;
        } else {
            $titleFieldName = 'title_de';
        }

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 's');
        $query->fields('s', array('psid', $titleFieldName, 'weight'));
        $query->condition('s.seg_type', $type);
        $query->condition('s.seg_parent', $seg_parent);
        $query->orderBy('s.weight', 'ASC');
        $result = $query->execute()->fetchAll();
        $datas = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $datas[$row->psid] = array(
                    'weight' => $row->weight,
                    'title' => $row->{$titleFieldName}
                );
            }
        }
        return $datas;
    }

    function itonics_segment_tr3_get_all_sub_segments_by_weight($type = 1) {

        global $language;

        if ($language->language == 'en' OR $language->language == 'de') {
            $titleFieldName = 'title_' . $language->language;
        } else {
            $titleFieldName = 'title_de';
        }

        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 's');
        $query->fields('s', array('psid', $titleFieldName, 'weight', 'seg_parent'));
        $query->condition('s.seg_type', $type);
        $query->condition('s.seg_parent', 0, '>');
        $query->orderBy('s.weight', 'ASC');
        $result = $query->execute()->fetchAll();
        $datas = array();

        if (!empty($result)) {
            foreach ($result as $row) {
                $datas[$row->psid] = array(
                    'weight' => $row->weight,
                    'title' => $row->{$titleFieldName},
                    'seg_parent' => $row->seg_parent
                );
            }
        }
        return $datas;
    }

}
