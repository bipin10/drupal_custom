<?php

/**
 * Segment Manage Form
 * @param array $form
 * @param type $form_state
 * @return type
 */
function itonics_segment_tr3_manage_form($form, $form_state, $type = 1) {

    _check_segment_type($type);

    $segment_title = _get_segment_title($type);

    // Set the breadcrum
    itonics_entity_details_set_breadcrumb(array(t('Start') => '<front>', t('Manage') . ' ' . $segment_title => 'manage/segment/' . $type));

    $form_state['segment_type'] = $type;

    if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
        return itonics_segment_tr3_manage_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['segments']));
    }
    $form['admin'] = itonics_segment_tr3_manage_list($type);
    return $form;
}

/**
 * Form Confirm : delete segments
 */
function itonics_segment_tr3_manage_multiple_delete_confirm($form, &$form_state, $segments, $is_parent=false) {
    global $language, $user;

    if ($language->language == 'en' OR $language->language == 'de') {
        $title = 'title_' . $language->language;
    } else {
        $title = 'title_de';
    }

    $segment_type = $form_state['segment_type'];

    $form['segments'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
    // array_filter returns only elements with TRUE values
    foreach ($segments as $key => $value) {
        $psid = $value;
        $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'seg');
        $query->addField('seg', $title);
        $query->condition('seg.psid', $psid);
        $query->groupBy('seg.' . $title);
        $seg_title = $query->execute()->fetchField();

        $form['segments'][$psid] = array(
            '#type' => 'hidden',
            '#value' => $psid,
            '#prefix' => '<li>',
            '#suffix' => check_plain($seg_title) . "</li>\n",
        );
    }

    $form['segment_type'] = array(
        '#type' => 'hidden',
        '#value' => $segment_type,
    );
    $form['seg_parent'] = array(
        '#type' => 'hidden',
        '#value' => $is_parent,
    );

    $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
    $form['#submit'][] = 'itonics_segment_tr3_manage_multiple_delete_confirm_submit';
    $confirm_question = format_plural(count($segments), 'Are you sure you want to delete this item?', 'Are you sure you want to delete these item?');
    return confirm_form($form, $confirm_question, 'manage/segment/' . $segment_type, t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Submit : segments
 */
function itonics_segment_tr3_manage_multiple_delete_confirm_submit($form, &$form_state) {

    $segment_type = $form_state['values']['segment_type'];

    if ($form_state['values']['confirm']) {
        itonics_segment_tr3_delete_multiple(array_keys($form_state['values']['segments']));

        $count = count($form_state['values']['segments']);
        watchdog('Segment', 'Deleted @count segments.', array('@count' => $count));
        drupal_set_message(format_plural($count, t('Selected Segment has been deleted.'), t('@count Segment have been deleted.')));
    }
    if(!empty($form_state['values']['seg_parent'])){
        $form_state['redirect'] = 'manage/sub-segment/' . $segment_type;
    }else{
        $form_state['redirect'] = 'manage/segment/' . $segment_type;
    }

}

/**
 * List of segments
 *
 */
function itonics_segment_tr3_manage_list($type) {
    global $language, $user;

    if ($language->language == 'en' OR $language->language == 'de') {
        $title = 'title_' . $language->language;
    } else {
        $title = 'title_de';
    }

    $segment_title = _get_segment_title($type);

    drupal_set_title(t('Manage') . ' ' . $segment_title);

    $admin_access = user_access(PERM_ADMINISTER_ITONICS_SEGMENT);

    //Add action link
    $add_link_url = l(t('Add') . ' ' . $segment_title, 'segment/create/' . $type);

    $form['add-action'] = array(
        '#markup' => '<div class="action-wrapper"><ul class="action-links"><li>' . $add_link_url . '</li></ul></div>',
    );

    //Add the bulk operation for news node type
    // Build the 'Update options' form.
    $form['options'] = array(
        '#type' => 'fieldset',
        //'#title' => t('Update options'),
        '#attributes' => array('class' => array('container-inline')),
        '#access' => $admin_access,
    );
    $options = array();
    $options = array('delete' => t('Delete selected Segment'));

    $form['options']['operation'] = array(
        '#type' => 'select',
        '#title' => t('Operation'),
        '#title_display' => 'invisible',
        '#options' => $options,
        '#default_value' => 'approve',
    );
    $form['options']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
        '#validate' => array('itonics_segment_tr3_manage_list_validate'),
        '#submit' => array('itonics_segment_tr3_manage_list_submit'),
    );

    $header = array(
        'title' => array('data' => t('Title'), 'field' => 'seg.' . $title, 'sort' => 'asc'),
        'changed' => array('data' => t('Changed'), 'field' => 'seg.changed', 'sort' => 'desc'),
        'operations' => array('data' => t('Operations')),
    );

    //Listing of trends for administration
    $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'seg');
    $query->fields('seg', array('psid', $title, 'changed'));
    $query->condition('seg.seg_type', $type);
    $query->groupBy('seg.psid')->groupBy('seg.' . $title)->groupBy('seg.changed');
    $count_query = clone $query;
    $count_query->addExpression('COUNT(seg.psid)');

    $query = $query->extend('PagerDefault')->extend('TableSort');
    $query
            ->fields('seg', array('psid', $title, 'changed'))
            ->condition('seg.seg_type', $type)
            ->limit(25)
            ->groupBy('seg.psid')->groupBy('seg.' . $title)->groupBy('seg.changed')
            ->orderByHeader($header);


    $result = $query->execute();

    $destination = drupal_get_destination();
    $options = array();

    foreach ($result as $row) {
        $trimmed_title = views_trim_text(array(
            'max_length' => 100,
            'ellipsis' => TRUE
                ), $row->{$title});
        $trimmed_title = html_entity_decode($trimmed_title);

        $options[$row->psid] = array(
            'title' => array('data' => array(
                    '#type' => 'link',
                    '#title' => $trimmed_title,
                    '#href' => "segment/$row->psid/details",
                )),
            'changed' => array('data' => format_date($row->changed, 'custom', 'd.m.Y'),),
            'operations' => array('data' => array(
                    array(
                        '#prefix' => '<ul class="links inline">',),
                    array(
                        '#prefix' => '<li>',
                        '#suffix' => '</li>',
                        '#type' => 'link',
                        '#title' => t('edit'),
                        '#href' => "segment/$row->psid/edit",
                        '#attributes' => array('class' => array('edit')),
                        '#options' => array('query' => $destination)),
                    array(
                        '#prefix' => '<li>',
                        '#suffix' => '</li>',
                        '#type' => 'link',
                        '#title' => t('delete'),
                        '#href' => "segment/$row->psid/delete",
                        '#attributes' => array('class' => array('delete')),
                        '#options' => array('query' => $destination)),
                    array(
                        '#suffix' => '</ul>',),
                )),
        );
    }

    // Only use a tableselect when the current user is able to perform any
    // operations.
    if ($admin_access) {
        $form['segments'] = array(
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $options,
            '#empty' => t('No Segment available.'),
        );
    }
    // Otherwise, use a simple table.
    else {
        $form['segments'] = array(
            '#theme' => 'table',
            '#header' => $header,
            '#rows' => $options,
            '#empty' => t('No Segment available.'),
        );
    }

    $form['pager']['#markup'] = theme('pager');
    return $form;
}

/**
 * Validate segments list form submissions.
 *
 * Check if any items have been selected to perform the chosen
 * 'Update option' on.
 */
function itonics_segment_tr3_manage_list_validate($form, &$form_state) {
    // Error if there are no items to select.
    // if (!is_array($form_state['values']['segments']) || !count(array_filter($form_state['values']['segments']))) {
    //     form_set_error('', t('No segment selected.'));
    // }

    if (!empty($form_state['values']['segments'])) {
        $any_selected = false;
        foreach ($form_state['values']['segments'] as $key => $value) {
            if ($value['checkbx'] > 0) {
                $any_selected = true;
            }
        }
    }

    if (!$any_selected) {
        form_set_error('', t('No segment selected.'));
    }
}

/**
 * Process segments List form submissions.
 *
 * Execute the chosen 'Update option' on the selected trends.
 */
function itonics_segment_tr3_manage_list_submit($form, &$form_state) {
    $operations = array(
        'publish' => array(
            'label' => t('Publish selected Segments'),
            'callback' => 'itonics_segment_tr3_mass_update',
            'callback arguments' => array('updates' => array('status' => 1)),
        ),
        'unpublish' => array(
            'label' => t('Unpublish selected Segments'),
            'callback' => 'itonics_segment_tr3_mass_update',
            'callback arguments' => array('updates' => array('status' => 0)),
        ),
        'delete' => array(
            'label' => t('Delete selected Segments'),
            'callback' => NULL,
        ),
    );

    $operation = $operations[$form_state['values']['operation']];
//    print_r($operation);die;
    // Filter out unchecked news

    $segments = array();
    foreach ($form_state['values']['segments'] as $key => $value) {
        if ($value['checkbx'] > 0) {
            $segments[] = $value['checkbx'];
        }
    }
    //$segments = array_filter($form_state['values']['segments']);
    if ($function = $operation['callback']) {
        // Add in callback arguments if present.
        if (isset($operation['callback arguments'])) {
            $args = array_merge(array($segments), $operation['callback arguments']);
        } else {
            $args = array($segments);
        }
        call_user_func_array($function, $args);

        cache_clear_all();
    } else {
        // We need to rebuild the form to go to a second step. For example, to
        // show the confirmation form for the news of trends.
        $form_state['rebuild'] = TRUE;
    }
}

/**
 * Segment Manage Form
 * @param array $form
 * @param type $form_state
 * @return type
 */
function itonics_segment_tr3_manage_dragable_form($form, $form_state, $type = 1, $is_parent = false) {

    $form['#attached']['js'] = array(
        drupal_get_path('module', 'itonics_segment_tr3') . '/js/manage_form.js',
    );

    _check_segment_type($type);

    $segment_title = _get_segment_title($type);

    itonics_entity_details_set_breadcrumb(array(t('Start') => '<front>', t('Manage') . ' ' . $segment_title => 'manage/segment/' . $type));

    $form_state['segment_type'] = $type;

    if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
        foreach ($form_state['values']['segments'] as $key => $value) {
            if ($value['checkbx'] > 0) {
                $segments[] = $value['checkbx'];
            }
        }
        return itonics_segment_tr3_manage_multiple_delete_confirm($form, $form_state, $segments, $is_parent);
    }
    $form['admin'] = itonics_segment_tr3_manage_dragable_list($type, $is_parent);
    return $form;
}

/**
 * List of segments for Dragable
 *
 */
function itonics_segment_tr3_manage_dragable_list($type, $is_parent) {
    global $language;
    if ($language->language == 'en' OR $language->language == 'de') {
        $title = 'title_' . $language->language;
    } else {
        $title = 'title_de';
    }

    $segment_title = _get_segment_title($type);

    if($type == 3 && !$is_parent){
      $segment_title = t('Merchandise Group');
    }elseif($type == 3 && $is_parent){
      $segment_title = t('Product Range');
    }

    /** Added By Santosh on 14 May 2017 starts**/
        $header_title = t('Manage') . ' ' . $segment_title;
    if(arg(1) == 'sub-segment' && $type != 3)
        $header_title = t('Manage') . ' Sub ' . $segment_title;

    if($is_parent && $type != 3){
        $add_text = t('Add Sub') . ' ' . $segment_title;
    } else{
        $add_text = t('Add') . ' ' . $segment_title;
    }

    if ($language->language == 'de') {
        $header_title = $segment_title . ' ' . t('Manage');
        if(arg(1) == 'sub-segment'){
            if(arg(2) == '1'){
                $segment_title = t('Trend'). ' ' .'Sub-Segment';
            }elseif(arg(2) == '2'){
                $segment_title = t('Technology'). ' ' .'Sub-Segment';
            }

            $header_title = $segment_title . ' ' . t('Manage');
        }
        if($is_parent && arg(1) == 'sub-segment'){
            if(arg(2) == '1'){
                $segment_title = t('Trend');
            }elseif(arg(2) == '2'){
                $segment_title = t('Technology');
            }
            $add_text =  $segment_title. ' ' .t('Add Sub');
        } else{
            $add_text = $segment_title . ' ' .t('Add');
        }
    }

    drupal_set_title($header_title);
    /** Added By Santosh on 14 May 2017 ends**/

    $admin_access = user_access(PERM_ADMINISTER_ITONICS_SEGMENT);

    //Add action link
    if ($is_parent) {
        $add_link_url = l($add_text, 'sub-segment/create/' . $type);
    } else {
        $add_link_url = l($add_text, 'segment/create/' . $type);
    }


    $form['add-action'] = array(
        '#markup' => '<div class="action-wrapper"><ul class="action-links"><li>' . $add_link_url . '</li></ul></div>',
    );

    //$admin_access = TRUE;
    //Add the bulk operation for news node type
    // Build the 'Update options' form.
    $form['options'] = array(
        '#type' => 'fieldset',
        // '#title' => t('Update options'),
        '#attributes' => array('class' => array('container-inline')),
        '#access' => $admin_access,
    );
    $options = array();
    $options = array('delete' => t('Delete selected Elements'));

    $form['options']['operation'] = array(
        '#type' => 'select',
        '#title' => t('Operation'),
        '#title_display' => 'invisible',
        '#options' => $options,
        '#default_value' => 'approve',
    );
    $form['options']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
        '#validate' => array('itonics_segment_tr3_manage_list_validate'),
        '#submit' => array('itonics_segment_tr3_manage_list_submit'),
    );


    $form['segments'] = array(
        '#type' => 'item',
        '#prefix' => '',
        '#suffix' => '<span class="loader"></span>',
        '#attributes' => array('class' => array('')),
        '#tree' => TRUE,
        '#theme' => 'segment_admin_form_draggable_theme'
    );


    $query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 's');

    $count_query = clone $query;
    $count_query->addExpression('COUNT(s.psid)');

    if ($is_parent) {
        $query->condition('s.seg_parent', 0, '>');
    } else {
        $count_query->condition('s.seg_parent', 0);
    }

    $query = $query->extend('PagerDefault');
    $query->fields('s', array('psid', $title, 'changed', 'weight','parent_field','seg_type','seg_parent'))
            ->condition('s.seg_type', $type)
            ->limit(100)
            ->orderBy('s.weight', 'ASC')
            ->setCountQuery($count_query);

    if ($is_parent) {
        $query->condition('s.seg_parent', 0, '>');
    } else {
        $query->condition('s.seg_parent', 0);
    }

    $result = $query->execute()->fetchAll();

    $destination = drupal_get_destination();

    if (!empty($result)) {
        foreach ($result as $row) {

            $form['segments'][$row->psid]['checkbx'] = array(
                '#type' => 'checkbox',
                '#return_value' => $row->psid,
                '#attributes' => array('class' => array('bu-select')),
            );

            $form['segments'][$row->psid]['title'] = array(
                '#markup' => (htmlspecialchars_decode($row->{$title}))
            );

            $form['segments'][$row->psid]['changed'] = array(
                '#markup' => format_date($row->changed, 'custom', 'd.m.Y')
            );
            if($row->parent_field > 1){
                $form['segments'][$row->psid]['parent'] = array(
                    '#markup' =>entity_get_controller(ITONICS_SEGMENT_TR3_ENTITY)->get_segment_title($row->parent_field)
                );
            }
            elseif ($row->seg_parent > 0){
                $form['segments'][$row->psid]['parent'] = array(
                    '#markup' =>entity_get_controller(ITONICS_SEGMENT_TR3_ENTITY)->get_segment_title($row->seg_parent)
                );
            }else{
                $form['segments'][$row->psid]['parent'] = array(
                    '#markup' =>'No Parent Field'
                );
            }
//            else{
//                $form['segments'][$row->psid]['parent'] = array(
//                    '#attributes' => "<div style='display:none'></div></div>"
//                );
//            }


            $form['segments'][$row->psid]['weight'] = array(
                '#type' => 'textfield',
                '#default_value' => $row->weight,
                '#size' => 3,
                '#attributes' => array('class' => array('position-weight'), 'psid' => $row->psid), // needed for table dragging
            );

            $markup_operation = '<ul class="links inline">'
                    . '<li>' . l(t('edit'), "segment/$row->psid/edit", array('query' => $destination, 'attributes' => array('class' => array('edit')))) . '</li>'
                    . '<li>' . l(t('edit'), "segment/$row->psid/delete", array('query' => $destination, 'attributes' => array('class' => array('delete')))) . '</li>'
                    . '</ul>';
            $form['segments'][$row->psid]['operations'] = array(
                '#access' => TRUE,
                '#markup' => $markup_operation,
            );
        }
    }

    $form['pager']['#markup'] = theme('pager');

    return $form;
}

/**
 * draggable theme for admin form
 * @param $vars
 * @return array
 */
function theme_segment_admin_form_draggable_theme($vars) {
    $element = $vars['element'];

    drupal_add_tabledrag('bu_reorder', 'order', 'sibling', 'position-weight'); // needed for table dragging

    $header = array(
        'checkbx' => array('data' => '<input type="checkbox" class="checkall"/>'),
        /*'title' => array('data' => t('Title'), 'field' => 'seg.title_en', 'sort' => 'asc'),
        'changed' => array('data' => t('Changed'), 'field' => 'seg.changed', 'sort' => 'desc'),*/
        'title' => array('data' => t('Title')),
        'changed' => array('data' => t('Changed')),
        'parent' => array('data' => t('Parent')),
        'weight' => array('data' => t('Position')),
        'operations' => array('data' => t('Operations')),
    );

    $rows = array();
    foreach (element_children($element) as $key) {
        $row = array();

        $row['data'] = array();
        foreach ($header as $fieldname => $title) {
            $row['data'][] = drupal_render($element[$key][$fieldname]);
        }
        $row['class'] = array('draggable'); // needed for table dragging
        $rows[] = $row;
    }
    return theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('id' => 'bu_reorder'), // needed for table dragging
    ));
}

/**
 * Ajax function to save the ordering of segment.
 *
 *
 */
function segment_page_admin_save_weight() {
    //getting all the bus according to their weights
    $weights = $_POST['weights'];
    //now saving their weights in the database

    if (!empty($weights)) {
        $bus = itonics_segment_tr3_load_multiple(array_keys($weights));
        foreach ($bus as $key => $bu) {
            $bu->weight = $weights[$bu->psid];
            segment_new($bu);
        }
    }
    exit();
}
