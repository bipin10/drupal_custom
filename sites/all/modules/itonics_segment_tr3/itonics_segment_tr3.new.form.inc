<?php

/**
 *
 * entity: itonics_segment_criteria_tr3
 *
 */

/**
 *
 * Form: Add/Edit form for itonics_segment_criteria_tr3
 *
 */
function itonics_segment_criteria_tr3_form( $form, &$form_state, $segment_criteria = null ) {

	drupal_add_library( 'system', 'ui.tabs' );

	$form['#attached']['js'][] = drupal_get_path( 'module', 'itonics_segment_tr3' ) . '/js/workspace_segment_form.js';

	if ( empty( $segment_criteria ) ) {
		$itonics_segment_criteria_tr3 = itonics_segment_tr3_new( 'itonics_segment_tr3' );
	} else {
		$form['psid'] = array(
			'#type'          => 'hidden',
			'#default_value' => $segment_criteria->psid,
		);

		drupal_set_title( 'segment Criteria: ' . html_entity_decode( $segment_criteria->title_en, ENT_QUOTES ) );
		$itonics_segment_criteria_tr3 = $segment_criteria;
	}

	$weight = 0;

	$form['segment_criteria'] = array(
		'#type'        => 'fieldset',
		'#prefix'      => '<ul class="slideshow-content" id="sc_2"><li><div id="all-interlinkage-wrapper">',
		'#suffix'      => '</div></li></ul>',
		'#collapsible' => false,
		'#collapsed'   => false,
		'#weight'      => $weight ++,
	);

	$form['segment_criteria']['slider_segments'] = array(
		'#type'   => 'item',
		'#prefix' => '<div id="form-questions">',
		'#suffix' => '</div>',
		'#weight' => $weight ++,
	);

	$form['segment_criteria']['slider_segments']['wrapper'] = array(
		'#type' => 'container',
	);

	$j = 1;

	$form['segment_criteria']['slider_segments']['wrapper']["psid_{$j}"] = array(
		'#type'  => 'hidden',
		'#value' => isset( $segment_criteria->psid ) ? $segment_criteria->psid : 0,
	);

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"] = array(
		'#type'        => 'fieldset',
		//'#title' => t('Slider') . ' ' . $j,
		'#collapsible' => true,
		'#collapsed'   => false,
		'#prefix'      => '<div class="question-fieldset-block"><div class="element-tab-wrapper form-elements">',
		'#suffix'      => '</div></div>',
	);

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"] = array(
		'#type'   => 'item',
		'#prefix' => '<div id="question-fields-' . $j . '">',
		'#suffix' => '</div>',
	);

	_itonics_segment_criteria_slider_segments_fields( $form, $form_state, $j, $segment_criteria );

	$j ++;

	$form['actions']            = array( '#type' => 'actions' );
	$form['actions']['#prefix'] = "<div class='control-wrapper'>";
	$form['actions']['#suffix'] = "</div>";
	$form['actions']['submit']  = array(
		'#type'   => 'submit',
		'#value'  => t( 'Save' ),
		'#prefix' => '<div class="save-btn cearfix">',
		'#suffix' => '</div>',
		'#weight' => $weight ++,
		'#ajax'   => array(
			'callback' => 'itonics_segment_criteria_add_form_ajax_submit',
			'wrapper'  => '',
			'event'    => 'click'
		),
	);

	return $form;
}

/**
 * Callback function to return with added options of selected question type
 *
 */
function segment_criteria_specific_segment_slider_editor_option_ajax_callback( $form, &$form_state ) {
	$click_button    = $form_state['triggering_element']['#name'];
	$click_button_ar = explode( "_", $click_button );
	if ( ! isset( $click_button_ar[2] ) || intval( $click_button_ar[2] ) <= 0 ) {
		return;
	}
	$j = intval( $click_button_ar[2] );

	return $form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * It just increments the max counter and causes a rebuild.
 */
function segment_criteria_specific_segment_slider_editor_add_one_single_option( $form, &$form_state ) {
	$click_button    = $form_state['triggering_element']['#name'];
	$click_button_ar = explode( "_", $click_button );
	if ( ! isset( $click_button_ar[2] ) || intval( $click_button_ar[2] ) <= 0 ) {
		return;
	}
	$j = intval( $click_button_ar[2] );
	$form_state["single_choice_option_counter_{$j}"] ++;
	$form_state['rebuild'] = true;
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * It just increments the max counter and causes a rebuild.
 */
function segment_criteria_specific_segment_slider_editor_remove_one_single_option( $form, &$form_state ) {
	$click_button    = $form_state['triggering_element']['#name'];
	$click_button_ar = explode( "_", $click_button );


	if ( ! isset( $click_button_ar[2] ) || intval( $click_button_ar[2] ) <= 0 ) {
		return;
	}
	$j = intval( $click_button_ar[2] );
	$form_state["single_choice_option_counter_{$j}"] --;
	$form_state['rebuild'] = true;
}

function _itonics_segment_criteria_slider_segments_fields( &$form, &$form_state, $j, $segment_slider = null ) {
	// ===================================================================
	//tabs html
	$tabs = '<div class="segment-criteria-tabs"><ul>';
	$tabs .= '<li><a id="taba-' . $j . '" href="#configtab-' . $j . '">' . t( 'Segment' ) . '</a></li>';
	$tabs .= '<li><a id="tabb-' . $j . '" href="#optiontab-' . $j . '">' . t( 'Sub Segment' ) . '</a></li>';
	$tabs .= '</ul>';

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["title_{$j}"] = array(
		'#type'          => 'textfield',
		'#size'          => 40,
		'#maxlength'     => 256,
		'#title'         => t( 'Title' ),
		'#id'            => "title_{$j}",
		'#default_value' => isset( $segment_slider->title_en ) ? html_entity_decode( $segment_slider->title_en, ENT_QUOTES ) : '',
		'#prefix'        => $tabs . '<div id="configtab-' . $j . '" class="configtab"><div class="title-input-wrapper-holder form-elements form-row  form-row-col-half"><div class="title-input-wrapper form-row-item-left en-flag"><div class="title-wrapper">' . t( "Title" ) . " <span class='required-field'>* </span><span class='icon-flag'></span></div>",
		'#suffix'        => '<div class="validation-error-msg" id="title_' . $j . '-error-msg"></div></div>',
		'#title_display' => 'invisible',
	);

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["title_de_{$j}"] = array(
		'#type'          => 'textfield',
		'#size'          => 40,
		'#maxlength'     => 256,
		'#title'         => 'Titel',
		'#id'            => "title_de_{$j}",
		'#default_value' => isset( $segment_slider->title_de ) ? html_entity_decode( $segment_slider->title_de, ENT_QUOTES ) : '',
		'#prefix'        => '<div class="title-input-wrapper form-row-item-right de-flag"><div class="title-wrapper">' . "Titel" . " <span class='required-field'>* </span><span class='icon-flag'></span></div>",
		'#suffix'        => '<div class="validation-error-msg" id="title_de_' . $j . '-error-msg"></div></div></div>',
		'#title_display' => 'invisible',
	);

	$form_state['prev_slider_title']    = isset( $segment_slider->title_en ) ? html_entity_decode( $segment_slider->title_en, ENT_QUOTES ) : '';
	$form_state['prev_slider_title_de'] = isset( $segment_slider->title_de ) ? html_entity_decode( $segment_slider->title_de, ENT_QUOTES ) : '';

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["slider_desc_{$j}"] = array(
		'#type'          => 'textarea',
		'#title'         => t( 'Description' ),
		'#default_value' => isset( $segment_slider->description_en ) ? $segment_slider->description_en : '',
		'#resizable'     => false,
		'#prefix'        => '<div class="title-input-wrapper-holder form-elements form-row form-row-col-half"><div class="title-input-wrapper form-row-item-left"><div class="title-wrapper">' . "Description" . " <span class='required-field'> </span></div>",
		'#suffix'        => '<div class="validation-error-msg" id="description_' . $j . '-error-msg"></div></div>',
		'#title_display' => 'invisible',
	);

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["slider_desc_de_{$j}"] = array(
		'#type'          => 'textarea',
		'#title'         => 'Beschreibung',
		'#default_value' => isset( $segment_slider->description_de ) ? $segment_slider->description_de : '',
		'#resizable'     => false,
		'#attributes'    => array( 'class' => array( 'slider_description_box' ) ),
		'#prefix'        => '<div class="title-input-wrapper form-row-item-right"><div class="title-wrapper">' . "Beschreibung" . " <span class='required-field'> </span></div>",
		'#suffix'        => '<div class="validation-error-msg" id="description_de_' . $j . '-error-msg"></div></div></div></div>',
		'#title_display' => 'invisible',
	);

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"] = array(
		'#type'   => 'item',
		'#prefix' => '<div id="optiontab-' . $j . '"><div id="question-options-' . $j . '" class="question-options-block">',
		'#suffix' => '</div>',
	);

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["single_choice_opt_heading_{$j}"] = array(
		'#markup' => t( 'Note: Leave blank if sub segment is not required' ) . '<p style="display:none" id="range-duplicate-slider-message-' . $j . '">' . t( 'Duplicate Entry not allowded' ) . '</p>',
		'#prefix' => '<h3 id="range-slider-message-' . $j . '" class="slider-project-form">',
		'#suffix' => '</h3>',
	);

	$i = 1;
	if ( $segment_slider ) {
		$options = entity_get_controller( ITONICS_SEGMENT_TR3_ENTITY )->get_sub_segments( $segment_slider->psid );

		foreach ( $options as $option ) {
			$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["single_choice_opt_id_{$j}_{$i}"] = array(
				'#type'  => 'hidden',
				'#value' => $option->psid,
			);

			$i ++;
		}
	}

	//fetch optios if any
	$i = 1;
	if ( empty( $form_state["single_choice_option_counter_{$j}"] ) && $segment_slider ) {

		if ( ! empty( $options ) ) {

			//$direction = "right";

			foreach ( $options as $option ) {

				//$direction = ($direction == "right") ? "left" : "right";

				$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["single_choice_opt_{$j}_{$i}"] = array(
					'#type'          => 'textfield',
					'#size'          => 30,
					'#maxlength'     => 256,
					'#title'         => 'Sub Segment' . " " . $i,
					'#id'            => "title_{$j}_{$i}",
					'#default_value' => $option->title_en,
					'#title_display' => 'invisible',
					'#prefix'        => '<div class="title-input-wrapper-holder form-elements form-row form-row-col-half"><div class="title-input-wrapper left"><div class="title-wrapper">' . 'Sub Segment' . " " . $i . " <span class='required-field'> </span></div>",
					'#suffix'        => '<div class="validation-error-msg" id="title_' . $j . '_' . $i . '-error-msg"></div></div>',
				);

				$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["single_choice_opt_de_{$j}_{$i}"] = array(
					'#type'          => 'textfield',
					'#size'          => 30,
					'#maxlength'     => 256,
					'#title'         => 'Untersegment' . " " . $i,
					'#id'            => "title_de_{$j}_{$i}",
					'#default_value' => $option->title_de,
					'#title_display' => 'invisible',
					'#prefix'        => '<div class="title-input-wrapper right"><div class="title-wrapper">' . 'Untersegment' . " " . $i . " <span class='required-field'> </span></div>",
					'#suffix'        => '<div class="validation-error-msg" id="title_de_' . $j . '_' . $i . '-error-msg"></div></div></div>',
				);

				$i ++;
			}
		}
	}

	if ( empty( $form_state["single_choice_option_counter_{$j}"] ) ) {
		$form_state["single_choice_option_counter_{$j}"] = ( $i > 1 ) ? $i - 1 : 2;
	}

	// This below hidden fields is  being used in preview case
	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["opt_counter_{$j}"] = array(
		'#type'          => 'hidden',
		'#name'          => "option_counter_{$j}",
		'#default_value' => $form_state["single_choice_option_counter_{$j}"],
		'#size'          => 5,
	);

	// $direction = "right";

	for ( ; $i <= $form_state["single_choice_option_counter_{$j}"]; $i ++ ) {

		// $direction = ($direction == "right") ? "left" : "right";

		$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["single_choice_opt_{$j}_{$i}"] = array(
			'#type'          => 'textfield',
			'#size'          => 30,
			'#maxlength'     => 256,
			'#title'         => 'Sub Segment' . " " . $i,
			'#default_value' => '',
			'#id'            => "title_{$j}_{$i}",
			'#title_display' => 'invisible',
			'#prefix'        => '<div class="title-input-wrapper-holder form-elements form-row form-row-col-half"><div class="title-input-wrapper left"><div class="title-wrapper">' . 'Sub Segment' . " " . $i . " <span class='required-field'> </span></div>",
			'#suffix'        => '<div class="validation-error-msg" id="title_' . $j . '_' . $i . '-error-msg"></div></div>',
		);

		$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["single_choice_opt_de_{$j}_{$i}"] = array(
			'#type'          => 'textfield',
			'#size'          => 30,
			'#maxlength'     => 256,
			'#title'         => 'Untersegment' . " " . $i,
			'#default_value' => '',
			'#id'            => "title_de_{$j}_{$i}",
			'#title_display' => 'invisible',
			'#prefix'        => '<div class="title-input-wrapper right"><div class="title-wrapper">' . 'Untersegment' . " " . $i . " <span class='required-field'> </span></div>",
			'#suffix'        => '<div class="validation-error-msg" id="title_de_' . $j . '_' . $i . '-error-msg"></div></div></div>',
		);
	}

	if ( $form_state["single_choice_option_counter_{$j}"] < 60 ) {
		$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["add_option_{$j}"] = array(
			'#type'       => 'submit',
			'#name'       => "add_option_{$j}",
			'#value'      => t( 'Add Sub Segment' ),
			'#attributes' => array( 'class' => array( 'btn-add-option' ) ),
			'#submit'     => array( 'segment_criteria_specific_segment_slider_editor_add_one_single_option' ),
			'#ajax'       => array(
				'callback' => 'segment_criteria_specific_segment_slider_editor_option_ajax_callback',
				'wrapper'  => 'question-options-' . $j,
				'method'   => 'html'
			),
			'#prefix'     => '<div class="add-option">',
			'#suffix'     => '</div>',
		);
	}

	if ( $form_state["single_choice_option_counter_{$j}"] > 2 ) {
		$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["options_{$j}"]["remove_option_{$j}"] = array(
			'#type'       => 'submit',
			'#name'       => "remove_option_{$j}",
			'#value'      => t( 'Remove Sub Segment' ),
			'#attributes' => array( 'class' => array( 'btn-add-option' ) ),
			'#submit'     => array( 'segment_criteria_specific_segment_slider_editor_remove_one_single_option' ),
			'#ajax'       => array(
				'callback' => 'segment_criteria_specific_segment_slider_editor_option_ajax_callback',
				'wrapper'  => 'question-options-' . $j,
				'method'   => 'html'
			),
			'#prefix'     => '<div class="remove-option">',
			'#suffix'     => '</div>',
		);
	}

	$form['segment_criteria']['slider_segments']['wrapper']["slider_segment_fieldset_{$j}"]["segment_fields_{$j}"]["expertise_{$j}"] = array(
		'#suffix' => '</div>',
	);
}

/**
 * Submit: itonics_segment_criteria_tr3_form
 */
function itonics_segment_criteria_add_form_ajax_submit( $form, &$form_state ) {
	try {

		$j = 1;
		// Each question type is must
		$slider_title = trim( check_plain( strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'title_' . $j ] ) ))));

		if ( trim( $slider_title ) == '' || trim( $slider_title ) == t( 'Enter Slider title' ) ) {
			form_set_error( 'title_' . $j );
			$second_page_error = true;
		} else {

			if ( isset( $form_state['values']["psid_1"] ) && $form_state['values']["psid_1"] > 0 ) {
				$duplicate_cnt = entity_get_controller( ITONICS_SEGMENT_TR3_ENTITY )->is_segment_exists( $slider_title );
				if ( $duplicate_cnt > 0 && $slider_title != $form_state['prev_slider_title'] ) {
					form_set_error( 'duplicate_title_' . $j );
					$second_page_error = true;
				}
			} else {
				$duplicate_cnt = entity_get_controller( ITONICS_SEGMENT_TR3_ENTITY )->is_segment_exists( $slider_title );
				if ( $duplicate_cnt > 0 ) {
					form_set_error( 'duplicate_title_' . $j );
					$second_page_error = true;
				}
			}
		}

		$slider_title_de = trim( check_plain( strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'title_de_' . $j ] ) ))));

		if ( trim( $slider_title_de ) == '' || trim( $slider_title_de ) == t( 'Enter Slider title' ) ) {
			form_set_error( 'title_de_' . $j );
			$second_page_error = true;
		} else {

			if ( isset( $form_state['values']["psid_1"] ) && $form_state['values']["psid_1"] > 0 ) {
				$duplicate_cnt = entity_get_controller( ITONICS_SEGMENT_TR3_ENTITY )->is_segment_exists( $slider_title_de );
				if ( $duplicate_cnt > 0 && $slider_title_de != $form_state['prev_slider_title_de'] ) {
					form_set_error( 'duplicate_title_de_' . $j );
					$second_page_error = true;
				}
			} else {
				$duplicate_cnt = entity_get_controller( ITONICS_SEGMENT_TR3_ENTITY )->is_segment_exists( $slider_title_de );
				if ( $duplicate_cnt > 0 ) {
					form_set_error( 'duplicate_title_de_' . $j );
					$second_page_error = true;
				}
			}
		}

		// NOTE: macrotrend is to sent at least two option filled
		// validation if already saved option being sent empty
		$cnt = 0;
		//Check how many option value have being filled
		for ( $i = 1; $i <= $form_state["single_choice_option_counter_{$j}"]; $i ++ ) {
			$option_label    = strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_' . $j . '_' . $i ])));
			$option_label_de = strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_de_' . $j . '_' . $i ])));
			if ( ! empty( $option_label ) && ! empty( $option_label_de ) ) {
				$cnt ++;
			}
		}

		// we add total valid value count to varaible @num
		// so we show error message as per its value
		$num = $cnt;

		$slider_options_unq_array    = array();
		$slider_options_unq_array_de = array();
		$duplicate_textfield         = array();
		$duplicate_textfield_de      = array();

		// IF not at least two option filled then show error msg
		/* TODO: Kiran*/
		/*if ($cnt < 2) {
			for ($k = 1; $k <= $form_state["single_choice_option_counter_{$j}"]; $k++) {
				$option_label = $form_state['values']['single_choice_opt_' . $j . '_' . $k];
				$option_label_de = $form_state['values']['single_choice_opt_de_' . $j . '_' . $k];
				if ($option_label == '' || $option_label_de == '') {

					if ($option_label == '') {
						form_set_error("title_{$j}_{$k}");
					}

					if ($option_label_de == '') {
						form_set_error("title_de_{$j}_{$k}");
					}

					$second_page_error = true;
					$num++;
				}
				if ($num == 2)
					break;
			}
		} else {
			for ($k = 1; $k <= $form_state["single_choice_option_counter_{$j}"]; $k++) {
				$option_label = $form_state['values']['single_choice_opt_' . $j . '_' . $k];
				$option_label_de = $form_state['values']['single_choice_opt_de_' . $j . '_' . $k];
				if ($option_label != '') {
					if (!empty($slider_options_unq_array)) {
						if (in_array($option_label, $slider_options_unq_array)) {
							form_set_error("duplicate_title_{$j}_{$k}");
							$duplicate_textfield[] = $k;
							$second_page_error = true;
						}
					}

					$slider_options_unq_array[] = $option_label;
				}

				if ($option_label_de != '') {
					if (!empty($slider_options_unq_array_de)) {
						if (in_array($option_label_de, $slider_options_unq_array_de)) {
							form_set_error("duplicate_title_de_{$j}_{$k}");
							$duplicate_textfield_de[] = $k;
							$second_page_error = true;
						}
					}

					$slider_options_unq_array_de[] = $option_label_de;
				}
			}
		}*/

		$clicka = false;
		$error  = form_get_errors();

		$commands = array();
		if ( $error ) {
			$commands[] = array( "command" => 'loadDefaultMrd' );

			$j = 1;

			if ( isset( $error[ 'title_' . $j ] ) ) {
				$commands[] = ajax_command_invoke( '#taba-' . $j, 'click' );
				$clicka     = true;
				$commands[] = ajax_command_invoke( '#title_' . $j, 'addClass', array( 'error' ) );
				$commands[] = ajax_command_html( '#title_' . $j . '-error-msg', t( 'Please enter the slider title EN.' ) );
			} else {
				$commands[] = ajax_command_invoke( '#title_' . $j, 'removeClass', array( 'error' ) );
				$commands[] = ajax_command_html( '#title_' . $j . '-error-msg', '' );

				if ( isset( $error[ 'duplicate_title_' . $j ] ) ) {
					$clicka     = true;
					$commands[] = ajax_command_invoke( '#taba-' . $j, 'click' );
					$commands[] = ajax_command_invoke( '#title_' . $j, 'addClass', array( 'error' ) );
					$commands[] = ajax_command_html( '#title_' . $j . '-error-msg', t( 'Slider name EN already exist.' ) );
				} else {
					$commands[] = ajax_command_invoke( '#title_' . $j, 'removeClass', array( 'error' ) );
					$commands[] = ajax_command_html( '#title_' . $j . '-error-msg', '' );
				}
			}

			if ( isset( $error[ 'title_de_' . $j ] ) ) {
				$commands[] = ajax_command_invoke( '#taba-' . $j, 'click' );
				$clicka     = true;
				$commands[] = ajax_command_invoke( '#title_de_' . $j, 'addClass', array( 'error' ) );
				$commands[] = ajax_command_html( '#title_de_' . $j . '-error-msg', t( 'Please enter the slider title DE.' ) );
			} else {
				$commands[] = ajax_command_invoke( '#title_de_' . $j, 'removeClass', array( 'error' ) );
				$commands[] = ajax_command_html( '#title_de_' . $j . '-error-msg', '' );

				if ( isset( $error[ 'duplicate_title_de_' . $j ] ) ) {
					$clicka     = true;
					$commands[] = ajax_command_invoke( '#taba-' . $j, 'click' );
					$commands[] = ajax_command_invoke( '#title_de_' . $j, 'addClass', array( 'error' ) );
					$commands[] = ajax_command_html( '#title_de_' . $j . '-error-msg', t( 'Slider name DE already exist.' ) );
				} else {
					$commands[] = ajax_command_invoke( '#title_de_' . $j, 'removeClass', array( 'error' ) );
					$commands[] = ajax_command_html( '#title_de_' . $j . '-error-msg', '' );
				}
			}

			// NOTE:  macrotrend is to sent at least two option filled
			// validation if already saved option being sent empty
			$cnt = 0;
			//Check how many option value have being filled
			for ( $i = 1; $i <= $form_state["single_choice_option_counter_{$j}"]; $i ++ ) {
				$option_label    = strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_' . $j . '_' . $i ])));
				$option_label_de = strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_de_' . $j . '_' . $i ])));
				if ( ! empty( $option_label ) && ! empty( $option_label_de ) ) {
					$cnt ++;
				}
			}

			// we add total valid value count to varaible @num
			// so we show error message as per its value
			$num = $cnt;

			// IF not at least two option filled then show error msg
			if ( $cnt < 2 ) {

				$commands[] = ajax_command_invoke( "#range-slider-message-{$j}", 'addClass', array( 'error-msg' ) );

				for ( $k = 1; $k <= $form_state["single_choice_option_counter_{$j}"]; $k ++ ) {
					$option_label    = strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_' . $j . '_' . $k ])));
					$option_label_de = strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_de_' . $j . '_' . $k ])));

					if ( $option_label == '' || $option_label_de == '' ) {


						$commands[] = ajax_command_invoke( "#title_{$j}_{$k}", 'removeClass', array( 'error' ) );
						$commands[] = ajax_command_html( "#title_{$j}_{$k}" . '-error-msg', '' );

						$commands[] = ajax_command_invoke( "#title_de_{$j}_{$k}", 'removeClass', array( 'error' ) );
						$commands[] = ajax_command_html( "#title_de_{$j}_{$k}" . '-error-msg', '' );

						if ( $option_label == '' ) {
							$commands[] = ajax_command_invoke( "#title_{$j}_{$k}", 'addClass', array( 'error' ) );
							$commands[] = ajax_command_html( "#title_{$j}_{$k}" . '-error-msg', t( 'Please fill the segment label EN.' ) );
						}

						if ( $option_label_de == '' ) {
							$commands[] = ajax_command_invoke( "#title_de_{$j}_{$k}", 'addClass', array( 'error' ) );
							$commands[] = ajax_command_html( "#title_de_{$j}_{$k}" . '-error-msg', t( 'Please fill the segment label DE.' ) );
						}

						$second_page_error = true;

						if ( $clicka == false ) {
							$commands[] = ajax_command_invoke( '#tabb-' . $j, 'click' );
						}

						$num ++;
					}
					if ( $num == 2 ) {
						break;
					}
				}
			} else {
				$commands[] = ajax_command_invoke( "#range-slider-message-{$j}", 'removeClass', array( 'error-msg' ) );

				$commands[] = ajax_command_invoke( "#title_{$j}_1", 'removeClass', array( 'error' ) );
				$commands[] = ajax_command_html( "#title_{$j}_1" . '-error-msg', '' );

				$commands[] = ajax_command_invoke( "#title_{$j}_2", 'removeClass', array( 'error' ) );
				$commands[] = ajax_command_html( "#title_{$j}_2" . '-error-msg', '' );

				$commands[] = ajax_command_invoke( "#title_de_{$j}_1", 'removeClass', array( 'error' ) );
				$commands[] = ajax_command_html( "#title_de_{$j}_1" . '-error-msg', '' );

				$commands[] = ajax_command_invoke( "#title_de_{$j}_2", 'removeClass', array( 'error' ) );
				$commands[] = ajax_command_html( "#title_de_{$j}_2" . '-error-msg', '' );
			}

			// IF not at least two option filled then show error msg
			if ( ! empty( $duplicate_textfield ) ) {
				for ( $i = 1; $i <= $form_state["single_choice_option_counter_{$j}"]; $i ++ ) {
					$commands[] = ajax_command_invoke( "#range-slider-message-{$j}", 'removeClass', array( 'error-msg' ) );
					$commands[] = ajax_command_invoke( "#title_{$j}_{$i}", 'removeClass', array( 'error' ) );
					$commands[] = ajax_command_html( "#title_{$j}_{$i}" . '-error-msg', '' );
				}

				if ( $clicka == false ) {
					$commands[] = ajax_command_invoke( '#tabb-' . $j, 'click' );
				}
				foreach ( $duplicate_textfield as $j => $k ) {
					$commands[] = ajax_command_invoke( "#title_1_{$k}", 'addClass', array( 'error' ) );
					$commands[] = ajax_command_invoke( "#range-slider-message-1", 'addClass', array( 'error-msg' ) );
					$commands[] = ajax_command_invoke( "#range-duplicate-slider-message-1", 'show' );
				}
			}

			// IF not at least two option filled then show error msg
			if ( ! empty( $duplicate_textfield_de ) ) {
				for ( $i = 1; $i <= $form_state["single_choice_option_counter_{$j}"]; $i ++ ) {
					$commands[] = ajax_command_invoke( "#range-slider-message-{$j}", 'removeClass', array( 'error-msg' ) );
					$commands[] = ajax_command_invoke( "#title_de_{$j}_{$i}", 'removeClass', array( 'error' ) );
					$commands[] = ajax_command_html( "#title_de_{$j}_{$i}" . '-error-msg', '' );
				}

				if ( $clicka == false ) {
					$commands[] = ajax_command_invoke( '#tabb-' . $j, 'click' );
				}
				foreach ( $duplicate_textfield_de as $j => $k ) {
					$commands[] = ajax_command_invoke( "#title_de_1_{$k}", 'addClass', array( 'error' ) );
					$commands[] = ajax_command_invoke( "#range-slider-message-1", 'addClass', array( 'error-msg' ) );
					$commands[] = ajax_command_invoke( "#range-duplicate-slider-message-1", 'show' );
				}
			}
		} else {

			global $user;
			$segment_slider_counter = 1;
			for ( $r = 1; $r <= $segment_slider_counter; $r ++ ) {

				$slider_name             = trim(strip_tags(html_entity_decode(strip_tags($form_state['values']["title_{$r}"]))));
				$slider_name_de          = trim(strip_tags(html_entity_decode(strip_tags($form_state['values']["title_de_{$r}"]))));
				$slider_name_abstract    = trim(strip_tags(html_entity_decode(strip_tags($form_state['values']["slider_desc_{$r}"]))));
				$slider_name_abstract_de = trim(strip_tags(html_entity_decode(strip_tags($form_state['values']["slider_desc_de_{$r}"]))));


				if ( ! empty( $slider_name ) ) {

					if ( isset( $form_state['values']["psid_{$r}"] ) && $form_state['values']["psid_{$r}"] > 0 ) {

						//  =============== UPDATE CASE ==================
						$psid = $form_state['values']["psid_{$r}"];

						// Insert the data
						$tb_name = 'itonics_segment_tr3';

						$data = array(
							'psid'           => $psid,
							'title_en'       => $slider_name,
							'title_de'       => $slider_name_de,
							'abstract_en'    => $slider_name_abstract,
							'abstract_de'    => $slider_name_abstract_de,
							'description_en' => $slider_name_abstract,
							'description_de' => $slider_name_abstract_de,
							'created'        => REQUEST_TIME,
							'changed'        => REQUEST_TIME,
							'uid'            => $user->uid
						);

						drupal_write_record( $tb_name, $data, 'psid' );

						$present_option_ids = array();

						$slider_option_counter = $form_state['values']["opt_counter_{$r}"];

						$existing_options = entity_get_controller( ITONICS_SEGMENT_TR3_ENTITY )->get_sub_segments( $psid );
						$existing_option_ids = array();

						foreach ($existing_options as $option) {
							$existing_option_ids[] = $option->psid;
						}

						for ( $m = 1; $m <= $slider_option_counter; $m ++ ) {

							if ( isset( $form_state['values'][ 'single_choice_opt_id_' . $r . '_' . $m ] ) && $form_state['values'][ 'single_choice_opt_id_' . $r . '_' . $m ] > 0 ) {

								$optid           = trim(strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_id_' . $r . '_' . $m ]))));
								$option_label    = trim(strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_' . $r . '_' . $m ]))));
								$option_label_de = trim(strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_de_' . $r . '_' . $m ]))));

								if ( $option_label != '' && $option_label_de != '' ) {
									db_update( $tb_name )// Table name no longer needs {}
									->fields( array(
										'title_en' => $option_label,
										'title_de' => $option_label_de,
										'changed'  => REQUEST_TIME,
										'uid'      => $user->uid
									) )
									->condition( 'psid', $optid )
									->execute();

									$present_option_ids[ $optid ] = $optid;
								} else {
									db_delete( $tb_name )->condition( 'psid', $optid )->execute();
								}


								// =======================
							} else {
								$option_label    = trim(strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_' . $r . '_' . $m ]))));
								$option_label_de = trim(strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_de_' . $r . '_' . $m ]))));
								if ( ! empty( $option_label ) && ! empty( $option_label_de ) ) {
									$data_option = array(
										'seg_parent'     => $psid,
										'title_en' => $option_label,
										'title_de' => $option_label_de,
										'changed'  => REQUEST_TIME,
										'uid'      => $user->uid
									);

									drupal_write_record( $tb_name, $data_option );
								}
							}
						}


						$results = array_diff( $existing_option_ids, $present_option_ids );

						foreach ( $results as $result ) {
							db_delete( $tb_name )->condition( 'psid', $result )->execute();
						}

					} else {
						//  =============== INSERT CASE ==================
						// Insert the data
						$tb_name = 'itonics_segment_tr3';

						$data = array(
							'title_en'       => $slider_name,
							'title_de'       => $slider_name_de,
							'abstract_en'    => $slider_name_abstract,
							'abstract_de'    => $slider_name_abstract_de,
							'description_en' => $slider_name_abstract,
							'description_de' => $slider_name_abstract_de,
							'created'        => REQUEST_TIME,
							'changed'        => REQUEST_TIME,
							'uid'            => $user->uid
						);

						drupal_write_record( $tb_name, $data );

						$rsid = $data['psid'];

						if ( $rsid > 0 ) {
							$slider_option_counter = $form_state['values']["opt_counter_{$r}"];

							for ( $m = 1; $m <= $slider_option_counter; $m ++ ) {
								$option_label    = trim(strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_' . $r . '_' . $m ]))));
								$option_label_de = trim(strip_tags(html_entity_decode(strip_tags($form_state['values'][ 'single_choice_opt_' . $r . '_' . $m ]))));
								if ( ! empty( $option_label ) && ! empty( $option_label_de ) ) {
									$data_option = array(
										'title_en'       => $option_label,
										'title_de'       => $option_label_de,
										'abstract_en'    => $option_label,
										'abstract_de'    => $option_label_de,
										'description_en' => $option_label,
										'description_de' => $option_label_de,
										'created'        => REQUEST_TIME,
										'seg_parent'     => $rsid,
										'changed'        => REQUEST_TIME,
										'uid'            => $user->uid
									);

									drupal_write_record( $tb_name, $data_option );
								}
							}
						}

					}
				}
			}

			// Notify the user.
			drupal_set_message( t( 'Successfully saved.' ) );
			$redirect_url = 'manage/segment';
			$commands[]   = ajax_command_invoke( null, 'redirect', array( $redirect_url ) );
		}
	} catch ( Exception $e ) {
		drupal_set_message( t( 'Some thing went wrong! Try again later!' ), 'error' );
	}

	print ajax_render( $commands );
	drupal_exit();
}

/*
 * Confirmation dialog box for deleting a segment_criteria
 */

function itonics_segment_criteria_tr3_delete_confirm( $form, &$form_state, $segment_criteria ) {
	$rc = $segment_criteria;
	itonics_entity_details_set_breadcrumb( array( t( 'Home' )                                                                                  => '<front>',
	                                              t( 'Segment Criteria: ' . html_entity_decode( $segment_criteria->title_en, ENT_QUOTES ) ) => '#'
	) );
	$count = itonics_rating_criteria_tr3_count( $rc->psid );
	$count = intval( $count );
	if ( $count == 0 ) {
		$form['#itonics_segment_criteria_tr3'] = $rc;
		$form['psid']                          = array( '#type' => 'value', '#value' => $rc->psid );

		return confirm_form( $form, t( 'Are you sure you want to delete %title ?', array( '%title' => html_entity_decode( $rc->title_en, ENT_QUOTES ) ) ), 'segment-criteria/' . $rc->psid . '/details', t( 'This action cannot be undone.' ), t( 'Delete' ), t( 'Cancel' )
		);
	} else {
		drupal_set_message( t( 'This segment Criteria "@criteria_name" cannot be deleted, segment criteria is being used.', array( '@criteria_name' => html_entity_decode( $rc->title_en, ENT_QUOTES ) ) ), 'error' );
		drupal_goto( 'manage/segment-criteria' );
	}

}

/**
 * Delete data are submitted here and deleted
 *
 * @param <type> $form
 * @param array $form_state
 */
function itonics_segment_criteria_tr3_delete_confirm_submit( $form, &$form_state ) {
	global $base_url;
	if ( $form_state['values']['confirm'] ) {
		$psid                         = $form_state['values']['psid'];
		$itonics_segment_criteria_tr3 = itonics_rating_criteria_tr3_load( $psid );

		$options = entity_get_controller( ITONICS_segment_CRITERIA_TR3_ENTITY )->get_slider_segment_options( $psid );

		foreach ( $options as $option ) {
			// Delete these values segment data from the segment tables, as these were unselected
			db_delete( 'itonics_segment_value' )
				->condition( 'optid', $option->optid )
				->execute();
		}

		// == Delete the options =======
		db_delete( 'itonics_segment_criteria_tr3_slider_options' )
			->condition( 'psid', $psid )
			->execute();

		// == Delete the options =======
		db_delete( 'itonics_segment_criteria_tr3_entity_rel' )
			->condition( 'psid', $psid )
			->execute();

		itonics_rating_criteria_tr3_delete( $form_state['values']['psid'] );
		watchdog( 'itonics_segment_criteria_tr3', 'segment Criteria: <em> %title </em> is deleted .', array( '%title' => $itonics_segment_criteria_tr3->title_en ) );
		drupal_set_message( t( 'segment Criteria %title has been deleted.', array( '%title' => html_entity_decode( $itonics_segment_criteria_tr3->title_en, ENT_QUOTES ) ) ) );
	}
	$form_state['redirect'] = '<front>';
}
