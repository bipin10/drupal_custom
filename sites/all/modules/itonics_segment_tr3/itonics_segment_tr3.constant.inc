<?php

/**
 * Define constants required by this module
 */
define('ITONICS_SEGMENT_TR3_ENTITY', 'itonics_segment_tr3');
define('ITONICS_SEGMENT_TR3_BASE_TABLE', 'itonics_segment_tr3');

/**
 * Defining Permission Constant
 */
define('PERM_ITONICS_SEGMENT_CREATE', 'create itonics_segment_tr3 entity');
define('PERM_ITONICS_SEGMENT_EDIT_ALL', 'edit all itonics_segment_tr3 entity');
define('PERM_ITONICS_SEGMENT_EDIT_OWN', 'edit own(self-created) itonics_segment_tr3 entity');
define('PERM_ITONICS_SEGMENT_DELETE_ALL', 'delete all itonics_segment_tr3 entity');
define('PERM_ITONICS_SEGMENT_DELETE_OWN', 'delete own(self-created) itonics_segment_tr3 entity');
define('PERM_ITONICS_SEGMENT_VIEW', 'view itonics_segment_tr3 entity');
define('PERM_ADMINISTER_ITONICS_SEGMENT', 'administer segment entity');


global $tr3_segment_types;

$tr3_segment_types = array(
		'1' => 'Trend',
		'2' => 'Technology',
    '3' => 'Inspiration'
	);