

<?php
 itonics_entity_details_set_breadcrumb(array(t('Start') => '<front>', t('Explorer') => 'innovation-wall/all', t('Segment: '.$segment->title_de) => 'segment/'.$segment->psid.'/details'));
?>

<div class='megatrend-tabs-wrapper'>
<?php echo $tabs_view; ?>
</div>

<div class='megatrend-reference-wrapper'>
    <?php echo $references; ?>
</div>
