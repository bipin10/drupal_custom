<!--render abstract block-->
<?php echo $vars['abstract']; ?>

<div class='right-tabs'>
    <?php echo $vars['tabs_view']; ?>
</div>


<div class="right-column-content">


    <div class="elementContent">
        <!--left-content-->
        <div class="element">
            <div class="elements desc">
                <h2><?php echo t('Description'); ?></h2>
                <?php
                if ($vars['description'] != '') {
                    echo $vars['description'];
                } else {
                    print t('N/a');
                }
                ?>
            </div>
        </div>
        <!--right-content -->
        <!--        <div class="elementSidebar">

                    <div class="region region-sidebar-right tags-wrapper">
                        <h2><?php //echo t('Tags');           ?></h2>
        <?php //echo itonics_vendor_tr3_tr3_taglist_htmllist(drupal_explode_tags($vars['tags']), t('No Tags Defined'));   ?>
                    </div>
                </div>-->
    </div>



