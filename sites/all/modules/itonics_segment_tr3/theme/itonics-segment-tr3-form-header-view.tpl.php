<script>

    var total_tabs = <?php if ((arg(0) == 'idea' || arg(0) == 'event' || arg(0) == 'opportunityspace' || arg(0) == 'challenge' || arg(0) == 'megatrend' || arg(0) == 'document' || arg(0) == 'technology') && arg(1) == 'create') { ?> '4'; <?php } elseif (arg(0) == 'segment') {
    ?> '2'; <?php } else {
    ?>'3'; <?php } ?>

    function rotate_slider(direction)
    {
        var trigger_cond = true;
        jQuery("#create-element-tab span.tab_item").each(function () {
            if (trigger_cond) {
                if (jQuery(this).hasClass('active'))
                {
                    var mrd = jQuery(this).attr('mrd');
                    var prev_mrd = parseInt(mrd) - 1;
                    var next_mrd = parseInt(mrd) + 1;
                    if (direction == "next")
                    {
                        var test = "mrd_" + next_mrd;
                        jQuery("#mrd_" + next_mrd).click();
                        jQuery("#mrd_" + mrd).removeClass("active");
                        jQuery("#mrd_" + next_mrd).addClass("active");
                    }
                    if (direction == "prev")
                    {
                        jQuery("#mrd_" + prev_mrd).click();
                        jQuery("#mrd_" + mrd).removeClass("active");
                        jQuery("#mrd_" + prev_mrd).addClass("active");
                    }
                    update_nav_visibility();
                    trigger_cond = false;
                }
            }
        });
    }

    function update_nav_visibility()
    {
        jQuery("#create-element-tab span.tab_item").each(function () {
            if (jQuery(this).hasClass('active'))
            {
                var mrd = jQuery(this).attr('mrd');
                if (mrd == 1)
                {
                    jQuery("#slideshow-previous").hide();
                    jQuery("#slideshow-next").show();
                    jQuery("#edit-actions").removeClass("lastSave");
                    jQuery('#citation-wrapper').removeClass("citation-from-active");
                } else if (mrd == 2 && total_tabs == 2)
                {
                    jQuery("#slideshow-next").hide();
                    jQuery("#slideshow-previous").show();
                } else if (mrd == 4 && total_tabs == 4)
                {
                    jQuery("#slideshow-next").hide();
                    jQuery("#slideshow-previous").show();
                    jQuery("#edit-actions").addClass("lastSave");
                    jQuery('#citation-wrapper').addClass("citation-from-active");
                } else if (mrd == 3 && total_tabs == 3)
                {
                    jQuery("#slideshow-next").hide();
                    jQuery("#slideshow-previous").show();
                    jQuery("#edit-actions").addClass("lastSave");
                    jQuery('#citation-wrapper').addClass("citation-from-active");
                } else
                {
                    jQuery("#slideshow-previous, #slideshow-next").show();
                    jQuery("#edit-actions").removeClass("lastSave");
                    jQuery('#citation-wrapper').removeClass("citation-from-active");
                }
            }

        });

        var trigger_cond = true;
        jQuery("#create-element-tab span.tab_item").each(function () {
            if (trigger_cond) {
                if (jQuery(this).hasClass('active'))
                {
                    var mrd = jQuery(this).attr('mrd');
                    jQuery("#sc_" + mrd).addClass("active");
                    var scroller_height = jQuery("#sc_" + mrd).height();
//                    if (mrd == 1)
//                    {
//                        var scroller_height = parseInt(scroller_height) + parseInt(20);
//                    }

                    var $el = jQuery("#slideshow-scroller");
                    $el.animate({height: scroller_height});
                    jQuery('#slideshow-scroller').css("height", scroller_height);
                    trigger_cond = false;
                }
            }
        });
    }

</script>
<div class="tabbedInterface">
    <div class='tabbed_content'>
        <div class='tabs' id="create-element-tab">
            <div class='moving_bg'></div>
            <span class='tab_item active' mrd="1" id="mrd_1">
                <?php echo t('General'); ?>
            </span>
            <span class='tab_item' mrd="2" id="mrd_2">
                <?php echo t('Details'); ?>
            </span>
            <?php if (!(arg(0) == 'segment')) { ?>
                <span class='tab_item' mrd="3" id="mrd_3">
                    <?php echo t('Relations'); ?>
                </span>
            <?php } ?>
            <?php if ((arg(0) == 'idea' || arg(0) == 'challenge' || arg(0) == 'event' || arg(0) == 'opportunityspace' || arg(0) == 'megatrend' || arg(0) == 'technology' || arg(0) == 'document') && ( arg(1) == 'create')) { ?>
                <span class='tab_item' mrd="4" id="mrd_4">
                <?php echo t('Reference'); ?>
                </span>
                <?php } ?>
        </div>
        <div class='slide_content' id="slideshow-scroller">
            <div class='tabslider' id="slideshow-holder">
