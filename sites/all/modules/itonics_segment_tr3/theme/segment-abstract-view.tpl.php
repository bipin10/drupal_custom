<?php
$abstract = autolink($abstract, array("target" => "_blank"));
drupal_add_css(libraries_get_path('fancybox') . '/fancybox/jquery.fancybox-1.3.4.css');
drupal_add_js(libraries_get_path('fancybox') . '/fancybox/jquery.fancybox-1.3.4.pack.js');
?>
<!-- Related elements -->
<div id="trend-abstract-wrapper" class="top-abstract-block">

    <?php if (!empty($header_image)) : ?>
        <div class="abstract-header-image">
            <a class="fancybox" rel="group" href="<?php echo $segment_full_image; ?>"><img src="<?php echo $header_image; ?>"/></a>
        </div>
    <?php endif; ?>

    <div class="abstract-right-column">

        <div class="abstract-abstract">
            <div class="abstract-title">
                <h2><?php echo t("Abstract"); ?></h2>
                <div class="entity-action-icons trend-meta">
                    <!-- Fav icon -->
<!--                    <div class = "favourites icon">
                        <a id="tr3_favourite_action-<?php //echo $segment->psid ?>" href="#" title="Add this element to your favorites" class="<?php //echo $alreadyFaved ? "is_favourite" : "no_favourite" ?>"
                           db-id="<?php //echo $segment->psid ?>" db-type="itonics_segment_tr3"> 
                        </a>
                    </div>-->
                    <!-- Export Link -->
                    <div class="exportmenu">
                        <ul class="clearfix icon-list">
                            <?php if (user_is_logged_in()) : ?>
                                <li class="print_print">
                                    <?php //echo $segment_export['print']; ?>
                                </li>
                            <?php endif; ?>
                            <!--              <li class="print_word">
                            <?php // echo $segment_export['word']; ?>
                                          </li>-->
                        </ul>
                    </div>
                </div><!-- end-action-icons -->

            </div>

            <!-- Description -->
            <div class="abstract-description">
                <?php echo $abstract; ?>      

            </div>
        </div>

        <div class="abstract-abstract-change-info trend-meta">
            <?php if (!empty($changed_date)) : ?>
                <div class="last-change">
                    <?php echo t('Last change') ?>: <?php echo $changed_date; ?>

                    <?php if (!user_is_anonymous()) : ?>
                        <?php if (!empty($segment_user)) : ?>
                            <span class="user-description">
                                <?php echo ' ' . $segment_user; ?></span>
                        <?php endif; ?>
                    <?php endif; ?>

                </div>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>

    </div>

</div>
<div class="clearfix"></div>

<script type="text/javascript">


    jQuery(window).load(function () {
        jQuery(".fancybox").fancybox();
    });



</script>