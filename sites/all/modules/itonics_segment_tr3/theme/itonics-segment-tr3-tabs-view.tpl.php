
<?php
$segment = $vars['segment'];
global $user;
?>

<div class='right-tabs'>
    <div class="tabs-wrapper" id="creation-tabs">
        <ul class="tabs clearfix">
            <li><?php echo l(t('Details'), 'segment/' . $segment->psid . '/details'); ?></li>
<!--            <li><?php //echo l(t('References'),'segment/'.$segment->psid. '/citations');   ?></li>-->
            <?php if (itonics_segment_tr3_access('edit', $segment)): ?>  <li class="tab-edit action-element"><?php echo l(t('Edit'), 'segment/' . $segment->psid . '/edit'); ?></li> <?php endif; ?>
            <?php if (itonics_segment_tr3_access('delete', $segment)): ?>  <li class="tab-delete action-element"><?php echo l(t('Delete'), 'segment/' . $segment->psid . '/delete'); ?></li> <?php endif; ?>

        </ul>
    </div>
</div>