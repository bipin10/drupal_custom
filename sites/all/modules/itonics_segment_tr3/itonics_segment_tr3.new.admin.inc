<?php

/**
 * Segment Manage Form
 * @param array $form
 * @param type $form_state
 * @return type
 */
function itonics_segment_tr3_new_manage_form($form, $form_state) {
	itonics_entity_details_set_breadcrumb(array(t('Home') => '<front>', t('Manage Segment') => 'manage/segment'));
	if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
		return itonics_segment_tr3_manage_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['segment_criterias']));
	}
	$form['admin'] = itonics_segment_tr3_manage_list();
	return $form;
}

/**
 * Form Confirm : delete segment_criterias
 */
function itonics_segment_tr3_manage_multiple_delete_confirm($form, &$form_state, $segment_criterias) {
	global $user;

	$form['segment_criterias'] = array('#prefix' => '<ul class="confirm_msg_list">', '#suffix' => '</ul>', '#tree' => TRUE);
	// array_filter returns only elements with TRUE values
	foreach (array_keys($segment_criterias) as $psid) {
		$query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'rc');
		$query->addField('rc', 'title_en');
		$query->condition('rc.psid', $psid);
		$query->groupBy('rc.title_en');
		$slider_title = $query->execute()->fetchField();

		$form['segment_criterias'][$psid] = array(
			'#type' => 'hidden',
			'#value' => $psid,
			'#prefix' => '<li><span class="itonics-icon picto-arrow-right"></span>',
			'#suffix' => $slider_title . "</li>\n",
		);
	}
	$form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
	$form['#submit'][] = 'itonics_segment_tr3_manage_multiple_delete_confirm_submit';
	$confirm_question = format_plural(count($segment_criterias), 'Are you sure you want to delete this Segment?', 'Are you sure you want to delete these Segment?');
	return confirm_form($form, $confirm_question, 'manage/segment', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Submit : segment_criterias
 */
function itonics_segment_tr3_manage_multiple_delete_confirm_submit($form, &$form_state) {
	if ($form_state['values']['confirm']) {

		foreach (array_keys($form_state['values']['segment_criterias']) as $psid) {

			// == Delete the options =======
			db_delete('itonics_segment_tr3')
				->condition('psid', $psid)
				->execute();

			// == Delete the options =======
			db_delete('itonics_segment_tr3')
				->condition('seg_parent', $psid)
				->execute();
		}

		// itonics_segment_tr3_delete_multiple(array_keys($form_state['values']['segment_criterias']));
		$count = count($form_state['values']['segment_criterias']);
		watchdog('Segment', 'Deleted @count BAFs.', array('@count' => $count));
		drupal_set_message(format_plural($count, 'Deleted 1 Segment.', 'Deleted @count Segments.'));
	}
	$form_state['redirect'] = 'manage/segment';
}

/**
 * List of segment_criterias
 *
 */
function itonics_segment_tr3_manage_list() {

	global $user;

	$user_roles = $user->roles;

	drupal_set_title(t('Manage Segment'));

	$admin_access = user_access(PERM_ADMINISTER_ITONICS_SEGMENT);

	//Add action link
	$add_link_url = l(t('Add Segment'), 'segment/create');

	$form['add-action'] = array(
		'#markup' => '<div class="action-wrapper"><ul class="action-links"><li>' . $add_link_url . '</li></ul></div>',
	);

	//Add the bulk operation for news node type
	// Build the 'Update options' form.
	$form['options'] = array(
		'#type' => 'fieldset',
		'#title' => t(''),
		'#attributes' => array('class' => array('container-inline')),
		'#access' => $admin_access,
	);

	$list_options = array('delete' => t('Delete selected Segment'));

	$form['options']['operation'] = array(
		'#type' => 'select',
		'#title' => t('Operation'),
		'#title_display' => 'invisible',
		'#options' => $list_options,
		'#default_value' => 'approve',
	);

	$form['options']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Update'),
		'#validate' => array('itonics_segment_tr3_manage_list_validate'),
		'#submit' => array('itonics_segment_tr3_manage_list_submit'),
	);

	$header = array(
		'title' => array('data' => t('Segment'), 'field' => 'rc.title_en', 'sort' => 'asc'),
		'sub_segment' => array('data' => t('Sub Segment')),
		'created' => array('data' => t('Created'), 'field' => 'rc.created', 'sort' => 'desc'),
		'operations' => array('data' => t('Operations')),
	);


	if(in_array('administrator', $user_roles)){ //if user role has administrator permission
		//Listing of trends for administration
		$query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'rc')->condition('seg_parent', 0);
		$count_query = clone $query;
		$count_query->addExpression('COUNT(rc.psid)');

		$query = $query->extend('PagerDefault')->extend('TableSort');
		$query
			->fields('rc', array('psid', 'title_en', 'created'))
			->condition('seg_parent', 0)
			->limit(25)
			//->groupBy('rc.psid')->groupBy('rc.title_en')->groupBy('rc.created')
			->orderByHeader($header)
			->setCountQuery($count_query);

		$result = $query->execute();
	}else{ //gets the project specific rating criteria
		$projid = _get_user_project_id();
		$query = db_select(ITONICS_SEGMENT_TR3_BASE_TABLE, 'rc');
		$query->fields('rc', array('psid', 'title_en', 'slider_desc', 'created'));
		$query->join('itonics_entity_user_project_rel', 'prjrel', 'rc.uid = prjrel.uid');
		$query->condition('prjrel.projid', $projid);
		$query->condition('seg_parent', 0);

		$query->groupBy('rc.psid')->groupBy('rc.title_en')->groupBy('rc.created');
		$count_query = clone $query;
		$count_query->addExpression('COUNT(rc.psid)');

		$query = $query->extend('PagerDefault')->extend('TableSort');
		$query
			->fields('rc', array('psid', 'title_en', 'created'))
			->condition('seg_parent', 0)
			->limit(50)
			->groupBy('rc.psid')->groupBy('rc.title_en')->groupBy('rc.created')
			->orderByHeader($header)
			->setCountQuery($count_query);

		$result = $query->execute()->fetchAll();
	}

	$destination = drupal_get_destination();
	$options = array();


	foreach ($result as $row) {

		$slider_title = views_trim_text(array('max_length' => 100, 'ellipsis' => TRUE), $row->title_en);
		$trimmed_title = $slider_title;


		$range_options = array();
		$sub_segments_options = entity_get_controller(ITONICS_SEGMENT_TR3_ENTITY)->get_sub_segments($row->psid);
		foreach ($sub_segments_options as $sub_option) {
			$range_options[] = $sub_option->title_en;
		}

		$options[$row->psid] = array(
			'title' => array('data' => $trimmed_title),
			'sub_segment' => array('data' => implode(', ', $range_options)),
			'created' => array('data' => itonics_entity_date_module($row->created)),
			'operations' => array('data' => array(
				array(
					'#prefix' => '<ul class="links inline">',),
				array(
					'#prefix' => '<li>',
					'#suffix' => '</li>',
					'#type' => 'link',
					'#title' => t('edit'),
					'#href' => "segment/$row->psid/edit",
					'#attributes' => array('class' => array('edit')),
					'#options' => array('query' => $destination)),
				array(
					'#prefix' => '<li>',
					'#suffix' => '</li>',
					'#type' => 'link',
					'#title' => t('delete'),
					'#href' => "segment/$row->psid/delete",
					'#attributes' => array('class' => array('delete')),
					'#options' => array('query' => $destination)),
				array(
					'#suffix' => '</ul>',),
			)),
		);
	}

	// Only use a tableselect when the current user is able to perform any
	// operations.
	if ($admin_access) {
		$form['segment_criterias'] = array(
			'#type' => 'tableselect',
			'#header' => $header,
			'#options' => $options,
			'#empty' => t('No rating criteria available.'),
		);
	}
	// Otherwise, use a simple table.
	else {
		$form['segment_criterias'] = array(
			'#theme' => 'table',
			'#header' => $header,
			'#rows' => $options,
			'#empty' => t('No rating criteria available.'),
		);
	}

	$form['pager']['#markup'] = theme('pager');
	return $form;
}

/**
 * Validate segment_criterias list form submissions.
 *
 * Check if any items have been selected to perform the chosen
 * 'Update option' on.
 */
function itonics_segment_tr3_manage_list_validate($form, &$form_state) {
	// Error if there are no items to select.
	if (!is_array($form_state['values']['segment_criterias']) || !count(array_filter($form_state['values']['segment_criterias']))) {
		form_set_error('', t('No Segment selected.'));
	}else{
		$selected_ids = array_filter($form_state['values']['segment_criterias']);
		$arr = array();
		foreach($selected_ids as $key => $rating_criteria_id):
			$query = db_select('itonics_segment_tr3_rel_advanced', 'sra')
						->fields('sra')
						->condition('segment_id', $rating_criteria_id);
			$count = $query->execute()->rowCount();

			if($count > 0){
				$arr[] = $rating_criteria_id;
			}

		endforeach;
		if(!empty($arr)){
			drupal_set_message(t('The selected Segment/s  cannot be deleted, Segment/s are being used.'), 'error');
			drupal_goto('manage/segment');
			//form_set_error('', t('The selected Segment/s  cannot be deleted, Segment/s are being used.'));
			//$form_state['redirect'] = 'manage/segment';
		}
	}
}

/**
 * Process segment_criterias List form submissions.
 *
 * Execute the chosen 'Update option' on the selected trends.
 */
function itonics_segment_tr3_manage_list_submit($form, &$form_state) {
	$operations = array(
		'publish' => array(
			'label' => t('Publish selected Segment'),
			'callback' => 'itonics_segment_tr3_mass_update',
			'callback arguments' => array('updates' => array('status' => 1)),
		),
		'unpublish' => array(
			'label' => t('Unpublish selected Segment'),
			'callback' => 'itonics_segment_tr3_mass_update',
			'callback arguments' => array('updates' => array('status' => 0)),
		),
		'delete' => array(
			'label' => t('Delete selected Segment'),
			'callback' => NULL,
		),
	);

	$operation = $operations[$form_state['values']['operation']];
	// Filter out unchecked news
	$segment_criterias = array_filter($form_state['values']['segment_criterias']);
	if ($function = $operation['callback']) {
		// Add in callback arguments if present.
		if (isset($operation['callback arguments'])) {
			$args = array_merge(array($segment_criterias), $operation['callback arguments']);
		} else {
			$args = array($segment_criterias);
		}
		call_user_func_array($function, $args);

		cache_clear_all();
	} else {
		// We need to rebuild the form to go to a second step. For example, to
		// show the confirmation form for the news of trends.
		$form_state['rebuild'] = TRUE;
	}
}