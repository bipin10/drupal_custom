<?php

/**
 * table view of teachers
 */
function teacher_list_page(){
    global $base_url;
    //add action link for create
    $add_link_url=l(t('Add Teacher'),'teachers/create');
    $form['add-action']=[
        '#markup'=>'<div class="action-wrapper"><ul class="action-links"><li>'.$add_link_url.'</li></ul></div>',
    ];
    $header=[
        'id'=>['data'=>t('Id'),'field'=>'t.id','sort'=>'asc'],
        'fname'=>['data'=>t('First Name'),'field'=>'t.firstname'],
        'lname'=>['data'=>t('Last Name'),'field'=>'t.lastname'],
        'operation'=>['data'=>t('Actions')]
    ];
    $query=db_select(TEACHER_BASE_TABLE,'t')
            ->fields('t');
    $query=$query->extend('PagerDefault')
            ->extend('TableSort')
            ->orderByHeader($header);
    $query->limit(5);
    $teachers=$query->execute()->fetchAll();
    
    $rows=[];
    foreach($teachers as $teacher){
        $edit_link=l(t('Edit'),$base_url.'/teachers/'.$teacher->id.'/edit');
        $view_link=l(t('View'),$base_url.'/teachers/'.$teacher->id.'/details');
        $delete_link=l(t('Delete'),$base_url.'/teachers/'.$teacher->id.'/delete');
        $rows[]=[
            'id'=>['data'=>$teacher->id],
            'fname'=>['data'=>$teacher->firstname],
            'lname'=>['data'=>$teacher->lastname],
            'operation'=>['data'=>$view_link.'&nbsp;&nbsp;'.$edit_link.'&nbsp;&nbsp;'.$delete_link]
        ];

    }
    $form['teachers']=[
        '#theme'=>'table',
        '#header'=>$header,
        '#rows'=>$rows,
        '#empty'=>t('No Teachers Available'),
    ];
    $form['pager']['#markup']=theme('pager');

    return $form;
}