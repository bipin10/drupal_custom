<?php
/**
 * include the constant file
 */
module_load_include('inc', 'teacher', 'teacher.constant');

/**
 * hook_access
 * it is used to define authorization and authentication for the users
 */
function teacher_access($op, $account = null)
{
    global $user;
    //this global variable is currently logged in user
    if(!in_array($op,['list','create','edit','view','delete'])){
        return false;
    }
    if(is_null($account)){
        $account=$user;
    }
    // super admin has all privileges
    if($user->uid==1){
        return true;
    }
    
    // if user has the access to adminstrate the students then allow all operations
    if(user_access(PERM_ADMINISTER_TEACHER,$account)){
        return true;
    }
    if($op=='list'){
        return user_access(PERM_LIST_TEACHER,$account);
    }
    if($op=='edit'){
        return user_access(PERM_EDIT_TEACHER,$account);
    }
    if($op=='create'){
        return user_access(PERM_CREATE_TEACHER,$account);
    }
    if($op=='view'){
        return user_access(PERM_VIEW_TEACHER,$account);
    }
    if($op=='list'){
        return user_access(PERM_DELETE_TEACHER,$account);
    }

    return false;

}

/**
 * hook_permission
 * this hook is used to define permissions for the module teacher
 * permissions will be shown in drupal backend
 * permissions will be checked in hook_access
 */
function teacher_permission()
{
    return [
        PERM_ADMINISTER_TEACHER => [
            'title' => t('Administer Teachers'),
            'restrict access' => true,
        ],
        PERM_LIST_TEACHER => [
            'title' => t('List Teachers'),
            'restrict access' => true,
        ],
        PERM_EDIT_TEACHER => [
            'title' => t('Edit Teachers'),
            'restrict access' => true,
        ],
        PERM_VIEW_TEACHER => [
            'title' => t('View Teachers'),
            'restrict access' => true,
        ],
        PERM_DELETE_TEACHER => [
            'title' => t('Delete Teachers'),
            'restrict access' => true,
        ],
        PERM_CREATE_TEACHER => [
            'title' => t('Create Teachers'),
            'restrict access' => true,
        ],
    ];
}
