<?php

/**
 * Define all constants
 */
define('TEACHER_BASE_TABLE','teacher');

define('PERM_ADMINISTER_TEACHER','administer teachers');

define('PERM_LIST_TEACHER','list teachers');

define('PERM_CREATE_TEACHER','create teachers');

define('PERM_EDIT_TEACHER','edit teachers');

define('PERM_DELETE_TEACHER','delete teachers');

define('PERM_VIEW_TEACHER','view teachers');




