<?php

/**
 * hook_form
 */
function teacher_create_form($form, &$form_state, $id = null)
{
    if (!is_null($id)) {
        $teacher = teacher_fetch_individual_data($id);
        $form['id'] = [
            '#type' => 'hidden',
            '#default_value' => $id,
        ];
    }
    $form['firstname'] = [
        '#type' => 'textfield',
        '#title' => t('First Name'),
        '#required' => true,
        '#default_value' => isset($teacher->firstname) ? $teacher->firstname : '',
    ];
    $form['lastname'] = [
        '#type' => 'textfield',
        '#title' => t('Last Name'),
        '#required' => true,
        '#default_value' => isset($teacher->lastname) ? $teacher->lastname : '',
    ];
    $form['submit'] = [
        '#type' => 'submit',
        '#name' => 'save',
        '#value' => t('Submit'),
    ];
    $form['cancel'] = [
        '#type' => 'submit',
        '#name' => 'cancel',
        '#value' => t('Cancel'),
    ];
    return $form;
}

/**
 * form data validation
 */
function teacher_create_form_validate($form, &$form_state)
{
    $form_data = (object) $form_state['values'];
    $triggering_element = $form_state['triggering_element']['#name'];

    if ($triggering_element == 'save') {

    } else {
        form_clear_error();
        unset($_SESSION['messages']['error']);
    }
}

/**
 * create form submit handler
 */

function teacher_create_form_submit($form, &$form_state)
{
    global $base_url;
    $triggering_element = $form_state['triggering_element']['#name'];

    if ($triggering_element == 'save') {
        $form_data = (object) $form_state['values'];
        // print('<pre>'.print_r($form_state['values'],1).'</pre>');die();
        if (property_exists($form_data, 'id')) {
            // if id is set it is update time
            $form_data->updated = REQUEST_TIME;
            drupal_write_record(TEACHER_BASE_TABLE, $form_data, 'id');
            $message = t('Teacher Updated!');
        } else {
            // if id is not set its new creation
            $form_data->created = REQUEST_TIME;
            drupal_write_record(TEACHER_BASE_TABLE, $form_data);
            $message = t('Teacher Created!');

        }
        drupal_set_message($message, 'stattus', false);
    }

    drupal_goto($base_url . '/teachers');
}
/**
 * callback function for deletion
 */
function teacher_delete_form($form, &$form_state,$id)
{
    $teacher=teacher_fetch_individual_data($id);
    $form['teacher']=[
        '#type'=>'value',
        '#value'=>$teacher
    ];
    $title=$teacher->firstname.' '.$teacher->lastname;
    return confirm_form($form,t('Are you sure you want to delete %title ?',['%title'=>$title]),'teachers',t('This operation can not be undone !!!'),t('Delete'),t('Cancel'));

}
/**
 * delete form submit handler
 */
function teacher_delete_form_submit($form,&$form_state){
    global $base_url;
    if($form_state['values']['confirm']){
        $teacher=$form_state['values']['teacher'];
        db_delete(TEACHER_BASE_TABLE)
            ->condition('id',$teacher->id)
            ->execute();
        drupal_set_message(t('Teacher Deleted successfully !!!'));
    }
    drupal_goto($base_url.'/teachers');
}
